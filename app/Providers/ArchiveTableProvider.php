<?php

namespace awwa\waschpi\App\Providers;

use Illuminate\Support\ServiceProvider;
use awwa\waschpi\Database\ArchiveStore;
use Illuminate\Support\Facades\Log;


/**
 * Provides the archive table.
 */
class ArchiveTableProvider extends ServiceProvider
{
    /**
     * Register archive store binding in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ArchiveStore::class, function ($app) {
            $storePath = env('APP_STORE');
            Log::info("provider register creates archive store: storePath=".$storePath);
            $Store = new ArchiveStore();
            $Store->setStorePath($storePath);
            return $Store;
        });
    }

}
