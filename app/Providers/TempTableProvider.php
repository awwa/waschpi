<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace awwa\waschpi\App\Providers;

use awwa\waschpi\Database\TempStore;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Log;


/**
 * Provides the temp table.
 *
 */
class TempTableProvider extends ServiceProvider
{

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(TempStore::class, function ($app) {
            $envStorePath = env('APP_STORE');
            $storePath = dirname(__FILE__,3).'/'.$envStorePath;
            TempStore::setStorePath($storePath);
            //Log::info("provider register creates temp store: storePath=".$storePath);
            $TempStore = new TempStore();
            return  $TempStore;
        });

    }

}
