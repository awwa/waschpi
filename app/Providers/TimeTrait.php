<?php
/**
 *
 * @todo make it a time provider
 * OR
 * @todo replace with Carbon library
 *
 * Licence: GPL2
 */
namespace awwa\waschpi\App\Providers;


/**
 * Sets and gets current time as timestamp.
 * @deprecated Carbon lib could be used instead
 */
trait TimeTrait
{
    /**
     * The mocked timestamp.
     * If null, the timestamp from time() will be used.
     * @var int
     */
    protected static $currentMockableTimeTimestamp = null;

    /**
     * Gets current time or mocked time.
     * @return int
     */
    public function getCurrentTime(): int
    {
        if(self::$currentMockableTimeTimestamp === null) {
            return time();
        }
        else {
            return self::$currentMockableTimeTimestamp;
        }
    }

    /**
     * Sets mocked time.
     * @param int $timestamp
     * @return void
     */
    public function setCurrentTime(int $timestamp): void
    {
        self::$currentMockableTimeTimestamp = $timestamp;
    }

    /**
     * Clears mocked time.
     * @return void
     */
    public static function clearMockedTime(): void
    {
        self::$currentMockableTimeTimestamp = null;
    }

}
