<?php
/**
 * Waschpi events are periodically taken snapshots of the washing machine parameters.
 */
namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

/**
 * Logs waschpi events
 */
class EventLog extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "events:log";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Logs a waschpi event";


    /**
     * Execute the console command.
     */
    public function handle()
    {
        // prevent script overrun by the next cron call
        ini_set('max_execution_time', 30);
        set_time_limit(30);

        $disabled = filter_var(env('APP_DISABLE_WASCHPI_EVENT_LOGGING'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        if ($disabled === true) {
            Log::notice("waschpi event logging disabled [{$this->signature}]");
        } else {
            Log::notice("start of command [{$this->signature}]");
            try {
                $Controller = new \App\Http\Controllers\CronController();
                $Response = $Controller->logEvent();
            } catch (Exception $exc) {
                $this->error("An error occurred ".$exc->getMessage());
            }
        }
    }

}



