<?php
/**
 * GPL2 Licence
 */
namespace App\Http\Controllers;

use awwa\waschpi\App\Events\WaschpiEvent;
use awwa\waschpi\Database\TempTable;
use awwa\waschpi\Database\ArchiveTable;
use Illuminate\Http\Request;

/**
 * Shows status of current wash cycle
 */
class StatusController extends Controller
{

    /**
     * Internal setting for amount of cycles to compare.
     * Higher values increase calculation time.
     * @var int
     */
    protected $countOfComparedHistoryCycles = 5;


    /**
     * Calculates best match for current wash cycle
     * @return array
     * @throws \Exception
     */
    public function findBestMatch()
    {
        $TempTable = app()->make(TempTable::class);
        $aStatus = $TempTable->getCurrentWashCycleStatus();

        $ArchiveTable = app()->make(ArchiveTable::class);
        $aCurrentOldestFirst = array_reverse($aStatus['currentEvents']);
        $aBestCycles = $ArchiveTable->findBestMatches($aCurrentOldestFirst, $this->countOfComparedHistoryCycles);

        $aResult = [];
        if (count($aBestCycles) >= 1) {
            $aBestCycle = array_shift($aBestCycles);
            if (isset($aBestCycle['metadata'])) {
                $aMeta = $aBestCycle['metadata'];
                $estimatedTimeToFinish = $aMeta['totalduration'] - count($aCurrentOldestFirst);

                $aResult = [
                    'endDateTime' => $aMeta['endDateTime']??null,
                    'totalduration' => $aMeta['totalduration']??null,
                    'maxTemp' => $aMeta['maxTemp']??null,
                    'maxRpm' => $aMeta['maxRpm']??null,
                    'remaining' => $estimatedTimeToFinish??null,
                    'startState' => $aStatus['startState']??null,
                    'currentState' => $aStatus['currentState']??null,
                    'elapsed' => $aStatus['elapsed']??null,
                ];

                if (filter_var(env('APP_DEBUG'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === true) {
                    $aResult['rawRpmSum'] = $aStatus['rawRpmSum']??null;
                }
            }
        }

        return $aResult;
    }

}
