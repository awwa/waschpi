<?php
/**
 * GPL2 Licence
 */
namespace App\Http\Controllers;


use awwa\waschpi\App\Events\WaschpiEvent;
use awwa\waschpi\Database\ArchiveStore;
use awwa\waschpi\Database\TempStore;
use awwa\waschpi\tests\WashCycleDataTrait;
use Illuminate\Http\Request;


/**
 * Simulates wash cycles.
 * For UI development
 */
class SimulationController extends Controller
{
    use WashCycleDataTrait;


    /**
     * Writes simulation data to temp store
     * @return array
     * @throws \Exception
     */
    public function writeSimulation(Request $Request)
    {
        $result = [];
        /** @var TempStore $TempTable */
        $TempTable = app()->make(TempStore::class);
        $TempTable->clearStore();

        $query = $Request->query;
        /** @var Symfony\Component\HttpFoundation\InputBag $query */
        $t = $query->get('time', 1600100000);
        $e = $query->get('exec', 90);
        $l = $query->get('length', 60);
        $this->createWashCycleSimulation($TempTable, $t, $e, $l);
        $TempTable->save();
        $all = $TempTable->getAllEvents();
        $result = $all;


        // $ArchiveTable = app()->make(ArchiveStore::class);
        // $aCurrentOldestFirst = array_reverse($aStatus['currentEvents']);
        // $aBestCycles = $ArchiveTable->findBestMatches($aCurrentOldestFirst, $this->countOfComparedHistoryCycles);



        return $result;
    }

}
