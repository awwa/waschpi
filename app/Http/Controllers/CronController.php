<?php
/**
 * Is called from cron @ 1min.
 * Logs data to the database and csv files.
 * Evaluates event list and harvests wash cycles.
 * Cleans temp table and wash cycle table
 * GPL2 Licence
 */
namespace App\Http\Controllers;

use awwa\waschpi\Database\TempStore;
use awwa\waschpi\Database\ArchiveStore;
use awwa\waschpi\App\Models\WashingMachine;
use awwa\waschpi\App\Events\WaschpiEvent;
use awwa\waschpi\App\Models\Washcycles\WashCycle;
use Illuminate\Support\Facades\Log;

/**
 * This controller organizes the periodic logging of washing machine data.
 * @todo the cleanup of the tables should be moved to a lumen queue or a laravel scheduler
 */
class CronController extends Controller
{
    /**
     * @var int timestamp of start time.
     */
    private $startTime;
    /**
     * @var int Exceeding this time (in seconds) should delay other tasks
     */
    private $maximumExpectedRuntime = 10;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->startTime = time();
    }

    /**
     * Fetches data from washing machine and stores it in database.
     * @return \Illuminate\Http\Response
     */
    public function logEvent()
    {
        // Log data from washing machine
        $TempTable = app()->make(TempStore::class);
        $WaMa = new WashingMachine();

        $PreviousEvent = $TempTable->getLastEvent();
        $Event = $WaMa->getData($PreviousEvent);
        $aResult = $this->save($Event);

        $Cycle = $this->harvestEvents();
        if($Cycle) {
            Log::info("washcycle was harvested, length=".$Cycle->getEventCount());
        }

        $ResponseFactory = new \Laravel\Lumen\Http\ResponseFactory();
        return $ResponseFactory->json($aResult);
    }

    /**
     * Saves given event to the database.
     * @param WaschpiEvent $Event
     * @return array
     */
    protected function save(WaschpiEvent $Event)
    {
        /** @var TempStore $TempTable */
        $TempTable = app()->make(TempStore::class);
        $InsertResult = $TempTable->addEvent($Event);
        $ts = $Event->getTimestamp();
        Log::debug("event written to database [$ts] ". date('c', $ts));
        $TempTable->save();

        return $InsertResult;
    }

    /**
     * Harvests the events of the temp table.
     * Moves finished wash cycles to the archive table.
     */
    public function harvestEvents():?WashCycle
    {
        // Move finished cycles to archive
        $TempTable = app()->make(TempStore::class);
        $aStats = null;
        $Cycle = $TempTable->harvestLastWashCyle($aStats);
        if ($Cycle) {
            Log::notice("harvested wash cycle at ". date('c'), $aStats);
            $ArchiveTable = app()->make(ArchiveStore::class);
            $count = $ArchiveTable->setWashCycle($Cycle);
        }
        return $Cycle;
    }

    /**
     * @todo this is work in progress
     * @todo this should be a lumen job or queue
     */
    public function cleanupStore()
    {
        // clean abundant event and cycle data at night
        $consumed = time() - $this->startTime;
        if ($consumed < $this->maximumExpectedRuntime) {
            $hours = intval(date('G'));
            $minutes = intval(date('m'));
            // at 3h every 10min
            if ($hours == 3 && !($minutes%10)) {
                $TempTable = app()->make(TempStore::class);
                $aResult = $TempTable->cleanExcessiveData(0, 200, false);
            }
            if ($hours == 4 && !($minutes%10)) {
                $ArchiveTable = app()->make(ArchiveStore::class);
                $aResult = $ArchiveTable->cleanExcessiveData(0, 30, false);
            }
        }
    }

}
