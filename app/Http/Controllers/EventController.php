<?php
/**
 * Events are store in the temp table.
 *
 * GPL2 Licence
 */
namespace App\Http\Controllers;

use awwa\waschpi\App\Events\WaschpiEvent;
use awwa\waschpi\App\Models\WashingMachine;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use awwa\waschpi\Database\TempStore;
use Exception;

/**
 * This controller organizes access of events.
 */
class EventController extends Controller
{
    /**
     * @var int
     */
    private $pageLength = 60;

    /**
     * Gets all events from temp table.
     */
    public function getList(Request $Request)
    {
        if ($Request->has('pageLength')) {
            $this->pageLength = $Request->input('pageLength');
            Log::debug("page length=". $this->pageLength);
        }

        /** @var TempStore $TempStore */
        $TempStore = app()->make(TempStore::class);

        $aResult = $TempStore->getAllEvents($this->pageLength);

        $ResponseFactory = new \Laravel\Lumen\Http\ResponseFactory();
        return $ResponseFactory->json($aResult);
    }

    /**
     * Get current measurements.
     * Is called from a route.
     */
    public function getCurrentMeasurements(Request $Request)
    {
        $WaMa = new WashingMachine();

        $WaschpiEvent = $WaMa->getData();
        $aResult = $WaschpiEvent->toArray();
        $ResponseFactory = new \Laravel\Lumen\Http\ResponseFactory();
        return $ResponseFactory->json($aResult);
    }



    /**
     * Get archived measurements.
     */
    public function getArchivedMeasurements(Request $Request)
    {
        throw new \Exception("todo: ".__METHOD__);
    }



}
