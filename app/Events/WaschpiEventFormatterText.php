<?php
/**
 * GPL2 Licence
 *
 */
namespace awwa\waschpi\App\Events;

use awwa\waschpi\App\Events\EventInterface;
use awwa\waschpi\App\Events\WaschpiEvent;



/**
 * Formats a waschpi event to a text line.
 */
class WaschpiEventFormatterText
{
    /**
     * Returns the event formatted as text line.
     * @param EventInterface|null $event
     * @return string
     */
    public function format(?EventInterface $event):string
    {
        if (!$event) {
            return "?";
        }
        $scaleFactor = 40;
        $str = date('m-d - H:i', $event->getTimestamp());
        $log = log($event->getRpm()+1, 10); // 1000 -> 3
        $padLength = intval(ceil($log * $scaleFactor));
        $str = str_pad($str, $padLength, '=', STR_PAD_RIGHT);
        $str .= "(".$event->getRpm().")";
        $str .= " T=". number_format($event->getDrumTemperature(), 0, ',', '.');
        return $str;
    }

}
