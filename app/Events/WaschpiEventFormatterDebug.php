<?php
/**
 *
 * GPL2 Licence
 */
namespace awwa\waschpi\App\Events;

use awwa\waschpi\Database\StorableInterface;
use awwa\waschpi\App\Events\EventInterface;
use awwa\waschpi\App\Events\WaschpiEvent;


/**
 * Formats a waschpi event.
 * @todo removing the washpi event member var would increase decoupling
 */
class WaschpiEventFormatterDebug
{
    /**
     * Returns the event formatted as debug output.
     * @param EventInterface|null $event
     * @return string
     */
    public function format(?EventInterface $event):string
    {
        if (!$event) {
            return "???";
        }

        $scaleFactor = 40;
        $str = date('H:i:s', $event->getTimestamp());
        $str .= " DC=".$event->getAbsoluteDrumCount();
        $str .= " RPM=".$event->getRpm();
        $str .= " T=". number_format($event->getDrumTemperature(), 0, ',', '.');
        return $str;
    }


}
