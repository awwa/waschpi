<?php
/**
 * GPL2 Licence
 *
 * @todo most probably, a waschpi event is not an event in the sense of this folder
 */
namespace awwa\waschpi\App\Events;

use awwa\waschpi\Database\StorableInterface;
use awwa\waschpi\App\Events\EventInterface;
use Exception;

/**
 * Represents an event of waschpi.
 * It contains temperatures and count of drum revolutions.
 * method WaschpiEvent fromArray(array $arr) creates from array
 */
class WaschpiEvent implements EventInterface, \JsonSerializable
{
    use \awwa\waschpi\App\Providers\TimeTrait;

    /**
     * @var int
     */
    protected $timestamp;
    /**
     * @var int
     */
    protected $absoluteDrumCount;

    /**
     * @var int
     */
    protected $rpm;

    /**
     * @var float
     */
    protected $controlBoxTemperature;

    /**
     * @var float
     */
    protected $drumTemperature;

    /**
     * @var float
     */
    protected $drumHumidity;

    /**
     * @var WaschpiEvent
     */
    protected $previousEvent;

    /**
     * @var WaschpiEvent
     */
    protected $nextEvent;


    public function __construct ()
    {
        $this->timestamp = $this->getCurrentTime();
    }

    /**
     * all values, without rpm
     */
    public static function create($timestamp, $drumCount, $boxTemp, $drumTemp, $drumHumid)
    {
        $self = new WaschpiEvent();

        $self->setTimestamp($timestamp);
        $self->setAbsoluteDrumCount($drumCount);
        $self->setControlBoxTemperature($boxTemp);
        $self->setDrumTemperature($drumTemp);
        $self->setDrumHumidity($drumHumid);

        return $self;
    }

    /**
     * @param type $arr
     * @return WaschpiEvent
     */
    public static function fromArray($arr): StorableInterface
    {
        $self = new WaschpiEvent();

        $self->setTimestamp($arr['timestamp']??null);
        $self->setDrumTemperature($arr['drumTemperature']??null);
        $self->setAbsoluteDrumCount($arr['absoluteDrumCount']??null);
        $self->setControlBoxTemperature($arr['controlBoxTemperature']??null);
        $self->setDrumHumidity($arr['drumHumidity']??null);

        if (isset($arr['rpm'])) {
            $self->setRpm($arr['rpm']);
        }

        return $self;
    }


    /**
     * Creates this Object from an array as it is read from a filestore.
     * Property-order is:
     *
     * timestamp
     * drumTemperature
     * absoluteDrumCount
     * rpm
     * controlBoxTemperature
     * drumHumidity
     *
     * @deprecated is not needed if datasets have keys
     *
     * @param array $iarr
     * @return WaschpiEvent
     */
    public static function fromIndexedArray($iarr): StorableInterface
    {
        $arr = [];
        $keys = [
            'timestamp',
            'drumTemperature',
            'absoluteDrumCount',
            //
            'rpm',
            'controlBoxTemperature',
            'drumHumidity',
        ];

        if (count($keys) !== count($iarr)) {
            throw new Exception(__FUNCTION__. "(): count of keys and values do not match");
        }
        reset($keys);
        reset($iarr);

        foreach($keys as $i => $key) {
            $arr[$key] = $iarr[$i];
        }

        $self = self::fromArray($arr);

        return $self;
    }


    public function toArray():array
    {
        $arr = [
            'timestamp' => $this->timestamp,
            'drumTemperature' => round($this->drumTemperature, 1),
            'absoluteDrumCount' => intval($this->absoluteDrumCount),
            //
            'rpm' => $this->getRpm(),
            'controlBoxTemperature' => round($this->controlBoxTemperature, 1),
            'drumHumidity' => round($this->drumHumidity, 0),
        ];

        return $arr;
    }

    public function getTimestamp(): int
    {
        return $this->timestamp;
    }

    public function getAbsoluteDrumCount(): int
    {
        return intval($this->absoluteDrumCount);
    }

    public function getRpm(): int
    {
        // calculate drum count
        $absoluteDrumCount = $this->getAbsoluteDrumCount();
        if ($this->rpm === null) {
            if ($this->previousEvent !== null) {
                $previousAbsoluteDrumCount = $this->previousEvent->getAbsoluteDrumCount();
                $this->rpm = $absoluteDrumCount - $previousAbsoluteDrumCount;
            }
            else {
                $this->rpm = 0;
            }
        }

        return $this->rpm;
    }

    public function getControlBoxTemperature(): float
    {
        return $this->controlBoxTemperature;
    }

    public function getDrumTemperature(): float
    {
        return $this->drumTemperature;
    }

    public function getDrumHumidity(): float
    {
        return $this->drumHumidity;
    }

    public function getPreviousEvent(): EventInterface
    {
        return $this->previousEvent;
    }

    public function getNextEvent(): EventInterface
    {
        return $this->nextEvent;
    }

    public function setTimestamp(?int $timestamp): void
    {
        $this->timestamp = $timestamp;
    }

    public function setAbsoluteDrumCount(?int $absoluteDrumCount): void
    {
        $this->absoluteDrumCount = $absoluteDrumCount;
    }

    public function setRpm(?int $rpm): void
    {
        $this->rpm = $rpm;
    }

    public function setControlBoxTemperature(?float $controlBoxTemperature): void
    {
        $this->controlBoxTemperature = $controlBoxTemperature;
    }

    public function setDrumTemperature(?float $drumTemperature): void
    {
        $this->drumTemperature = $drumTemperature;
    }

    public function setDrumHumidity(?float $drumHumidity): void
    {
        $this->drumHumidity = $drumHumidity;
    }

    public function setPreviousEvent(?EventInterface $previousEvent): void
    {
        $this->previousEvent = $previousEvent;
    }

    public function setNextEvent(?EventInterface $nextEvent): void
    {
        $this->nextEvent = $nextEvent;
    }


    public function jsonSerialize()
    {
        return $this->toArray();
    }

}
