<?php
/**
 * Events are like messages containing information about e.g. washcycle status.
 * GPL2 Licence
 */

namespace awwa\waschpi\App\Events;

use awwa\waschpi\Database\StorableInterface;


/**
 * Describes an event.
 */
interface EventInterface extends StorableInterface
{
}
