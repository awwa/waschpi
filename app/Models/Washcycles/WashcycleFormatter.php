<?php
/**
 * GPL2 Licence
 *
 */
namespace awwa\waschpi\App\Models\Washcycles;

use awwa\waschpi\App\Events\EventInterface;
use awwa\waschpi\App\Events\WaschpiEvent;
use awwa\waschpi\Database\StorableInterface;
use awwa\waschpi\App\Models\Washcycles\WashCycle;


/**
 * Formats a washcycle.
 */
class WashcycleFormatter
{
    /**
     * @var WashCycle
     */
    protected $object;

    /**
     * Sets the object to format.
     * @param EventInterface|null $object
     */
    public function __construct($object)
    {
        $this->object = $object;
    }

    /**
     * Returns formatted as debug output.
     * @return string
     */
    public function toDebugOutput():array
    {
        if (!$this->object) {
            return [];
        }
        $object = $this->object;

        $arr = $object->getMetaInfo();
        return $arr;
    }

}
