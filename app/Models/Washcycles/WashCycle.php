<?php
/**
 * GPL2 Licence
 */
declare(strict_types=1);
namespace awwa\waschpi\App\Models\Washcycles;

use awwa\waschpi\App\Events\EventInterface;
use awwa\waschpi\App\Events\WaschpiEvent;
use awwa\waschpi\Database\StorableInterface;


/**
 * Represents a completed wasch cycle.
 *
 * It contains meta data and all the events of that cycle.
 *
 * The order of data is oldest first.
 * method WashCycle fromArray(array $arr) creates from array
 */
class WashCycle implements StorableInterface, \Countable, \JsonSerializable
{
    use \awwa\waschpi\App\Providers\TimeTrait;

    const P_META_INFO = 'metainfo';
    const P_EVENT_SERIES = 'eventseries';

    /**
     * mongo id of this cycle
     * @var string
     */
    protected $strId;
    /**
     * @var array
     */
    protected $metaInfo = [];
    /**
     * A series of waschpi events.
     * Sorted oldest first.
     * @var WaschpiEvent[]
     */
    protected $eventSeries = [];

    /**
     * Creates from a list of raw events.
     *
     * Order is kept: oldest first
     * @param WaschpiEvent[] $arr  Expects oldest first
     * @return WashCycle
     */
    public static function createFromWashCycleArray(array $arr): WashCycle
    {
        $Cycle = new WashCycle();
        $maxTimestamp = 0;
        foreach ($arr as $key => $Event) {
            $currentTS = $Event->getTimestamp();
            if ($currentTS > $maxTimestamp) {
                $maxTimestamp = $currentTS;
            }
            $Cycle->addEvent($Event);
        }
        $Cycle->setupMetaInfo(['timestamp' => $maxTimestamp]);
        return $Cycle;
    }

    /**
     * Creates cycle from ist storable form.
     * @param array $arr
     * @return StorableInterface
     */
    public static function fromArray(array $arr): StorableInterface
    {
        $Cycle = new WashCycle();
        $Cycle->fromArrayMetaInfo($arr);
        $Cycle->fromArrayEventSeries($arr);

        return $Cycle;
    }


    public function fromArrayMetaInfo(array $arr)
    {
        $this->strId = strval($arr['_id']??null);
        $this->metaInfo = $arr;
        unset($this->metaInfo[self::P_EVENT_SERIES]);
    }


    public function fromArrayEventSeries(array $arr)
    {
        $jsonSeries = $arr[self::P_EVENT_SERIES]??"[]";
        $aSeries = json_decode($jsonSeries, true);
        foreach ($aSeries as $key => $event) {
            $Event = WaschpiEvent::fromArray($event);
            $this->addEvent($Event);
        }
    }

    /**
     * Converts in its storable form.
     * Events must be serialized for mongodb on raspberry pi.
     * @return array
     */
    public function toArray():array
    {
        $arr[self::P_META_INFO] = $this->toArrayMetaInfo();
        // It should be noted here, that raspberrypi's mongodb
        // appears not to work with structured data (nesting level > 1).
        // Therefore, the data series are json encoded
        $arr[self::P_EVENT_SERIES] = $this->toArrayEventSeries();
        return $arr;
    }

    public function toArrayMetaInfo()
    {
        return $this->metaInfo;
    }

    public function toArrayEventSeries()
    {
        $arr = [];
        foreach ($this->eventSeries as $key => $Event) {
            /** @var WaschpiEvent $Event */
            $arr[] = $Event->toArray() ;
        }
        return $arr;
    }

    public function addEvent(EventInterface $Event)
    {
        $this->eventSeries[] = $Event;
        $this->setMetaInfoKey('eventCount', $this->count());
    }

    /**
     * Compiles characteristic values of this wash cycle.
     *
     * Characteristic numbers are:
     * total duration, duration of prewash, duration of mainwash,
     * maximum temperature, accumulated drum count,
     *
     * @param array $aMetaInfo
     */
    public function setupMetaInfo(array $aMetaInfo=[])
    {
        $this->metaInfo = array_merge($this->metaInfo, $aMetaInfo);
        $count = count($this->eventSeries);
        $this->setMetaInfoKey('totalduration',  $count);

        $minTime = PHP_INT_MAX;
        $maxTime = 0;
        $minDrumTemp = 10000;
        $maxDrumTemp = 0;
        $minHumidity = 1000;
        $maxHumidity = 0;
        $maxRpm = 0;
        foreach ($this->eventSeries as $Event) {
            $temperatureDiff = $Event->getDrumTemperature() > $maxDrumTemp;

            $value = $Event->getTimestamp();
            if ($value > $maxTime) {
                $maxTime = $value;
            }
            if ($value < $minTime) {
                $minTime = $value;
            }

            $value = $Event->getDrumTemperature();
            if ($value > $maxDrumTemp) {
                $maxDrumTemp = $value;
            }
            if ($value < $minDrumTemp) {
                $minDrumTemp = $value;
            }

            $value = $Event->getDrumHumidity();
            if ($value > $maxHumidity) {
                $maxHumidity = $value;
            }
            if ($value < $minHumidity) {
                $minHumidity = $value;
            }

            $value = $Event->getRpm();
            if ($value > $maxRpm) {
                $maxRpm = $value;
            }
        }
        $format = 'Y-m-d H:i:s';
        $this->setMetaInfoKey('startTimestamp', $minTime);
        $this->setMetaInfoKey('endTimestamp', $maxTime);
        $this->setMetaInfoKey('startDateTime', date($format, $minTime));
        $this->setMetaInfoKey('endDateTime', date($format, $maxTime));

        $this->setMetaInfoKey('minTemp', $minDrumTemp);
        $this->setMetaInfoKey('maxTemp', $maxDrumTemp);

        $this->setMetaInfoKey('minHumidty', $minHumidity);
        $this->setMetaInfoKey('maxHumidity', $maxHumidity);

        $this->setMetaInfoKey('maxRpm', $maxRpm);
    }

    public function setMetaInfoKey(string $key, $value)
    {
        $this->metaInfo[$key] = $value ;
    }

    public function getMetaInfoKey(string $key)
    {
        return $this->metaInfo[$key]??null;
    }

    /**
     * Compares this wash cycle with the events of temp table.
     */
    public function compareWithTempTable($TempTable)
    {
        // reverse order to oldest first (like wash cycle)
        $aCurrentEvents = $TempTable->getAllEvents();

        $aResult = $this->compareSerieWithSweep($aCurrentEvents);
        return $aResult['smallestDelta'];
    }

    /**
     * Compares this wash cycle with the given events.
     * Compares each given event with each event of this cycle.
     * The offset is shifted and all runs are stored.
     * The smalles delta is the result.
     *
     * @param WaschpiEvent[] $aCurrentEvents  Order is oldest first (like wash cycle)
     * @return array 'smallestDelta' Smallest delta of all runs.
     * @return array 'partialResults' Partial results of offset runs
     */
    public function compareSerieWithSweep($aCurrentEvents)
    {
        $aDeltaSum = []; // indexed by offset
        $aResult = [];
        $aOffsetResults = [];

        // loop runs with 19 offsets: -9 to +9
        for ($offset = -20; $offset <= 9; $offset++) {
            $aPartialResult = $this->compareSerieWithOffset($aCurrentEvents, $offset);
            $aDeltaSum[$offset] = $aPartialResult['deltasum']??null;
            $aOffsetResults[$offset] = [
                'deltasum' => $aPartialResult['deltasum']??null,
                'loopCount' => $aPartialResult['loopCount'],
            ];
        }
        $aResult['partialResults'] = $aOffsetResults;

        // the smallest delta is the the best match
        $smallestDelta = PHP_INT_MAX;
        $smallestDeltaOffset = null;
        foreach ($aDeltaSum as $offset => $deltaSum) {
            if ($deltaSum < $smallestDelta) {
                $smallestDelta = $deltaSum;
                $smallestDeltaOffset = $offset;
            }
        }
        $aResult['smallestDelta'] = $smallestDelta;
        $aResult['smallestDeltaOffset'] = $smallestDeltaOffset;

        return $aResult;
    }

    /**
     * Compares this wash cycle with the given reference events and offset.
     *
     * Compares each given event with each event of this cycle.
     * The time is shifted and all deltas are summed up.
     *
     * @param array $aCurrentEvents  Order is oldest first (like wash cycle)
     * @param int $offset Offset moves the pointer of the referenced events.
     * @return array 'offset'
     * @return array 'break_current_at'
     * @return array 'break_ref_at'
     * @return array 'loopCount'
     * @return array 'deltalist'
     * @return array 'deltasum_overall'
     * @return array 'deltasum' This is the very result: Averaged delta per event
     * @return array 'deltalist'
     */
    public function compareSerieWithOffset(array $aCurrentEvents, int $offset)
    {
        $washCycleId = $this->strId;
        // maximum calculation range
        $segment = 180;
        // the sum of all deltas
        $overallDeltaSum = 0;
        $aResult = [];
        $aResult['offset'] = $offset;
        $aDeltas = [];

        // a wash cycle in this object is always completed,
        // that is why the own data series is always the reference.
        // cycle data should be sorted oldest first
        $aRefEvents = $this->eventSeries;
        // fix order of data
        $order = WashCycle::getSortOrder($aRefEvents);
        if ($order[0] > $order[1]) {
            // order is newest first: reverse needed
            $aRefEvents = array_reverse($aRefEvents);
        }

        $i = 0;
        $iRef = 0;
        if ($offset < 0) {
            // pointer can't be negative, so the current series pointer is moved
            // in opposite direction (forward)
            $i = intval($offset * -1);
        }
        elseif ($offset > 0) {
            $iRef = $offset;
        }

        $loopCount = 0;
        $currentTS = 0;
        $prevReferenceTS = 0;
        while (true) {
            // runs until one of the two data lines ends
            // 1) current
            /** @var WaschpiEvent $CurrentEvent */
            $CurrentEvent = $aCurrentEvents[$i]??null;
            if ($CurrentEvent === null) {
                $aResult['break_current_at'] = $i;
                break;
            }
            if ($currentTS && $currentTS > $CurrentEvent->getTimestamp()) {
                throw new \Exception("CURRENT[$washCycleId]: wrong sorted data, prev ts is greater than current");
            }
            $currentTS = $CurrentEvent->getTimestamp();

            // 2) reference
            /** @var WaschpiEvent $RefEvent */
            $RefEvent = $aRefEvents[$iRef]??null;
            if ($RefEvent === null) {
                $aResult['break_ref_at'] = $iRef;
                break;
            }
            if ($prevReferenceTS && $prevReferenceTS > $RefEvent->getTimestamp()) {
                throw new \Exception("REF[$washCycleId]: wrong sorted data, prev [$prevReferenceTS] ts is greater than current ".$RefEvent->getTimestamp());
            }
            $prevReferenceTS = $RefEvent->getTimestamp();

            // delta of rpm and temperature of the investigated event pair
            $rpmDelta = $RefEvent->getRpm() - $CurrentEvent->getRpm();
            $tempDelta = $RefEvent->getDrumTemperature() - $CurrentEvent->getDrumTemperature();
            $deltaSum = abs($rpmDelta) + abs($tempDelta);
            $deltaQuad = pow($rpmDelta, 2) + pow($tempDelta, 2);
            $aDeltas[] = "$i $rpmDelta, $tempDelta";
//            $aDeltas[] = $RefEvent->getRpm() ." ". $CurrentEvent->getRpm();

            $overallDeltaSum += $deltaSum;

            $loopCount++;
            $i++;
            $iRef++;
        }
        $aResult['loopCount'] = $loopCount;
        $aResult['deltalist'] = $aDeltas;

        // delta per event in average
        if($loopCount) {
            $aResult['deltasum_overall'] = $overallDeltaSum;
            $averageDeltasum = $overallDeltaSum / $loopCount;
            $aResult['deltasum'] = $averageDeltasum;
        }

        return $aResult;
    }

    /**
     * For countable interface
     * @return int
     */
    public function count() : int
    {
        return count($this->eventSeries);
    }

    // setter/getter

    public function getMetaInfo(): array
    {
        $this->setupMetaInfo();
        return $this->metaInfo;
    }

    /**
     * Sorted oldest first.
     * @return array
     */
    public function getEventSeries(): array
    {
        return $this->eventSeries;
    }

    public function setMetaInfo(array $metaInfo): void
    {
        $this->metaInfo = $metaInfo;
    }

    public function setEventSeries(array $eventSeries): void
    {
        $this->eventSeries = $eventSeries;
    }

    /**
     * Returns mongo db id.
     *
     * It should be noted here, that raspberrypi's mongodb
     * appears not to work (e.g. delete) with mongo object ids
     *
     * @return string
     */
    public function getDbId(): string
    {
        return strval($this->strId);
    }

    public function getTimestamp()
    {
        return $this->getMetaInfoKey('timestamp');
    }

    public function getEventCount()
    {
        return count($this->getEventSeries());
    }


    /**
     * calculates sort order
     * @param WaschpiEvent[] $aEvents
     */
    public static function getSortOrder(array $aEvents):array
    {
        $countOldestFirst = 0;
        $countNewestFirst = 0;
        /** @var WaschpiEvent $PrevEvent */
        $PrevEvent = null;
        foreach ($aEvents as $key => $Event) {
            /** @var WaschpiEvent $Event */
            if ($PrevEvent) {
                $prev = $PrevEvent->getTimestamp();
                $current = $Event->getTimestamp();
                if ($prev > $current) {
                    // descending, sorted newest first
                    $countNewestFirst++;
                }
                elseif ($prev < $current) {
                    // ascending, sorted oldest first
                    $countOldestFirst++;
                }
                else {
                    throw new \Exception("data error, both timestamps are equal");
                }
            }

            $PrevEvent = $Event;
        }
        return [$countNewestFirst, $countOldestFirst];
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

}
