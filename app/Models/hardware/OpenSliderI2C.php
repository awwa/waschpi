<?php
/*
 * copied from Openslider I2C class
 */
namespace awwa\waschpi\App\Models\hardware;

use RuntimeException;

class OpenSliderI2C
{
    const MODE_SINGLE_VALUE = 'b';

    const MODE_WORD = 'w';

    /** @var string */
    protected $bus;

    /** @var string */
    protected $chipAddress;

    /**
     * i2c constructor.
     *
     * @param int $bus
     * @param int $chipAddress
     */
    public function __construct(int $bus, int $chipAddress)
    {
        $this->bus = $bus;
        $this->chipAddress = $chipAddress;
    }

    /**
     * @param int $dataAddress
     * @param string $mode
     * @param bool $bSwapBytes
     * @return string
     */
    public function get(int $dataAddress, string $mode = self::MODE_SINGLE_VALUE, bool $bSwapBytes=false) : string
    {
        $command = 'i2cget -y ' . (
                escapeshellarg($this->bus) . ' ' .
                escapeshellarg($this->chipAddress) . ' ' .
                escapeshellarg($dataAddress) . ' '
            ) . $mode;

        $hex = $this->execute($command);
        $int = intval($hex, 16);

        if ($mode === self::MODE_WORD && $bSwapBytes) {
            // swap high and lowbyte
            $intLow = $int & 0x00FF;
            $intHigh = $int & 0xFF00;

            $intHigh2 = $intLow * 0x100;
            $intLow2 = $intHigh / 0x100;

            $int = $intHigh2 + $intLow2;
            $int = strval($int);
        }

        return $int;
    }

    /**
     * @param int    $dataAddress
     * @param int    $value
     * @param string $mode
     */
    public function set(int $dataAddress, int $value, string $mode = self::MODE_SINGLE_VALUE) : void
    {
        $command = 'i2cset -y ' . (
                escapeshellarg($this->bus) . ' ' .
                escapeshellarg($this->chipAddress) . ' ' .
                escapeshellarg($dataAddress) . ' ' .
                escapeshellarg($value) . ' '
            ) . $mode;

        $this->execute($command);
    }

    /**
     * @param int $dataAddress
     * @param int $value
     */
    public function setWord(int $dataAddress, int $value) : void
    {
        $this->set($dataAddress, $value, self::MODE_WORD);
    }

    /**
     * @param int $dataAddress
     *
     * @return int
     */
    public function getWord(int $dataAddress) : int
    {
        return $this->get($dataAddress, self::MODE_WORD);
    }

    /**
     * @param $command
     *
     * @return ?string
     */
    protected function execute($command) : ?string
    {
        $result = shell_exec($command . ' 2>&1');

        if (strncmp($result, 'Error:', 6) === 0) {
            throw new RuntimeException($result);
        }

        return $result;
    }
}

