<?php
/**
 * I2C is a communication bus.
 *
 * A i2c class should provide these methods:
 *
 * blockWrite()
 *
 * quickCommand()
 *
 * sendByte()
 *
 * receiveByte()
 *
 * writeByte()
 *
 * readByte()
 *
 * readWord()
 *
 * writeWord()
 *
 *
 * GPL2 Licence
 */

namespace awwa\waschpi\App\Models\hardware;

/**
 * This class extends the open slider I2C class by some minor bug fixes and
 * byte-swap capability of word-read.
 * It also adds a BCD read-mode.
 */
class I2C extends OpenSliderI2C {

    protected const MODE_NO_VALUE = 'c';

    /**
     * Sets bus number and chip address.
     *
     * @param int $bus
     * @param int $chipAddress
     */
    public function __construct(int $bus, int $chipAddress = 0) {
        parent::__construct($bus, $chipAddress);
    }

    /**
     * Calls i2cdetect from i2c tools.
     */
    public function detect(): string {
        $command = 'i2cdetect -y ' . (
                escapeshellarg($this->bus)
                );

        return $this->execute($command);
    }

    /**
     * Sends a quick command.
     * No value possible.
     * @param int $command
     * @param string $mode
     * @return void
     */
    public function quickCommand(int $command): void {
        $command = 'i2cset -y ' . (
                escapeshellarg($this->bus) . ' ' .
                escapeshellarg($this->chipAddress) . ' ' .
                escapeshellarg($command) . ' '
                ) . self::MODE_NO_VALUE;

        $this->execute($command);
    }

    /**
     * @param int $dataAddress
     * @param int $value
     * @param bool $bBcdMode
     * @return int Returns the set value.
     * @throws \Exception
     */
    public function writeByte(int $dataAddress, int $value, bool $bBcdMode = false)
    {
        if ($bBcdMode) {
            if ($value >= 100) {
                $msg = "Exceeds bcd format: [$value]";
                throw new \Exception($msg);
            }

            // 11: 1 1
            // 0: 0 0
            // 9: 0 9
            $tenth = floor($value/10);
            $ones = $value % 10;
            $bcd = $tenth << 4;
            $bcd += $ones;
            $set = $bcd;
        }
        else {
            $set = $value;
        }

        $this->set($dataAddress, $set);

        return $value;
    }

    /**
     * @param int $dataAddress
     * @param bool $bBcdMode
     * @return int
     */
    public function readByte(int $dataAddress, bool $bBcdMode = false, string &$raw=null): int {
        $command = 'i2cget -y ' . (
                escapeshellarg($this->bus) . ' ' .
                escapeshellarg($this->chipAddress) . ' ' .
                escapeshellarg($dataAddress) . ' '
                ) . 'b';

        $raw = $this->execute($command);
        $raw = trim($raw);
        $strHex = $raw;

        if ($bBcdMode) {
            $strHex = trim($strHex);
            $aMatches = null;
            $nMatches = preg_match("/^0x(\d)(\d)$/", $strHex, $aMatches);
            if (!$nMatches) {
                $msg = "not a bcd number: [$strHex]";
                throw new \Exception($msg);
            }
            $tenth = intval($aMatches[1]); // decimals
            $ones = intval($aMatches[2]); // ones
            $int = $tenth*10 + $ones;
        } else {
            $int = intval($strHex, 16);
        }

        return $int;
    }

    /**
     * Gets value without giving an address on the chip.
     * @param string $mode
     * @return string
     */
    public function getNoAddress(string $mode = null): string {
        $command = 'i2cget -y ' . (
                escapeshellarg($this->bus) . ' ' .
                escapeshellarg($this->chipAddress) . ' '
                ) . $mode ?: '';

        $hex = $this->execute($command);
        $int = intval($hex, 16);

        return $int;
    }

    /**
     * Swaps the bytes of a word.
     * Value must be given as hex string.
     * Required for e.g. LM75
     * @param string $word
     * @return string
     */
    protected function swapWordBytes(string $word): string {
        $int = intval($word, 16);

        // swap high and lowbyte
        $intLow = $int & 0x00FF;
        $intHigh = $int & 0xFF00;

        $intHigh2 = $intLow * 0x100;
        $intLow2 = $intHigh / 0x100;

        $int = $intHigh2 + $intLow2;
        $int = strval($int);

        return $int;
    }

}
