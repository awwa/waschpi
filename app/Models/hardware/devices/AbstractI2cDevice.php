<?php
/**
 * Every device is connected to a bus and has an address.
 * GPL2 Licence
 */
namespace awwa\waschpi\App\Models\hardware\devices;

use awwa\waschpi\App\Models\hardware\I2C;

/**
 * This class may be a base for device classes.
 *
 */
abstract class AbstractI2cDevice
{
    /**
     * @var int
     */
    protected $busNumber;

    /**
     * @var I2C
     */
    protected $i2c;

    /**
     * @var int
     */
    protected $address;


    abstract public function getName(): string;

    abstract public function getAddress(): int;

    /**
     * Returns a unique identifier for this device.
     * @return string
     */
    public function getSignature()
    {
        $signature = $this->getName().' @ '.$this->getAddress();
        return $signature;
    }

    /**
     * Sets the bus number (0 or 1).
     * @deprecated Usually you would use create().
     * @param int $busNumber
     */
    public function __construct(int $busNumber)
    {
        $this->busNumber = $busNumber;
    }

    /**
     * Sets the bus number (0 or 1) and address offset.
     *
     * @param int $busNumber
     * @param int $addressOffset
     * @return \self
     */
    public static function create(int $busNumber, int $addressOffset=0)
    {
        $device = new static($busNumber);
        $device->setAddressOffset($addressOffset);
        return $device;
    }

    /**
     * Creates and returns the i2c connection.
     * @param int $chipAddress
     * @return I2C
     */
    public function getConnection(int $chipAddress): I2C
    {
        if ($this->i2c === null) {
            $this->i2c = new I2C($this->busNumber, $chipAddress);
        }
        return $this->i2c;
    }

    public function getBusNumber(): int
    {
        return $this->busNumber;
    }

    public function setBusNumber(int $busNumber): void
    {
        $this->busNumber = $busNumber;
    }

    public function setI2c(I2C $i2c): void
    {
        $this->i2c = $i2c;
    }

    protected function getI2c(): I2C
    {
        return $this->i2c;
    }

    public function setAddress(int $address): void
    {
        $this->address = $address;
    }


    /**
     * writes debug data to a csv file.
     *
     * The file is named like: debug-addrXX.csv where XX is the address of the device.
     *
     * If the file is missing, no debug data will be written.
     * @param array $data
     * @throws \Exception
     */
    protected function debugReadings(array $data)
    {
        // debug
        $aCsvData = array_values($data);
        $filename = 'debug-addr'.$this->getAddress().'.csv';
        $filename = dirname(__FILE__, 3).'/install/logs/'.$filename;
        if (is_writable($filename)) {
            $fp = fopen($filename, 'a');
            $written = fputcsv($fp, $aCsvData, chr(9));
            if ($written === false) {
                throw new \Exception("writing csv failed for [$filename]");
            }
            fclose($fp);
        }
        else {
            // no debug of readings
        }
    }

}