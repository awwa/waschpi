<?php
/**
 * The PCF8593 is a CMOS clock and calendar circuit, optimized for low power consumption.
 *
 * The built-in word address register is incremented automatically after each written
 * or read data byte. The built-in 32.768 kHz oscillator circuit and the first 8 bytes of the RAM
 * are used for the clock, calendar, and counter functions. The next 8 bytes can be
 * programmed as alarm registers or used as free RAM space.
 *
 * Event counter mode is selected by bits 4 and 5 which are logic 10 in the control and status
 * register. The event counter mode is used to count pulses externally applied to the
 * oscillator input (OSCO left open-circuit).
 *
 * In the event-counter mode, events are stored in BCD format. D5 is the most significant
 * and D0 the least significant digit. The divider is by-passed.
 *
 * Every device is connected to a bus and has an address.
 * 
 * A3h (163d) for reading, A2h (162d) for writing.
 *
 * The I2C address is: 51h / 81d
 *
 * GPL2 Licence
 */

namespace awwa\waschpi\App\Models\hardware\devices;

use awwa\waschpi\lib\I2C;

/**
 * This class represents a PCF 8593 clock / counter.
 *
 * It uses a i2c connection
 *
 * @method PCF8593 create(int $busNo) creates a counter chip
 */
class PCF8593 extends AbstractI2cDevice implements DeviceInterface
{
    use AddressSelectionTrait;

    protected const BASE_ADDRESS = 0x51;

    // Registers go from 0 to F
    protected const REGISTER_CONTROL_AND_STATUS = 0x00;
    // Only registers 1 to 3 are used here
    protected const REGISTER_BCD_COUNTER_D1_D0 = 0x01; // LSB
    protected const REGISTER_BCD_COUNTER_D3_D2 = 0x02;
    protected const REGISTER_BCD_COUNTER_D5_D4 = 0x03; // MSB

    protected const REGISTER_RAM_0 = 0x0C;
    protected const REGISTER_RAM_1 = 0x0D;
    protected const REGISTER_RAM_2 = 0x0E;

    // Bitmasks for control flags
    protected const CONTROL_ENABLE_ALARM = 0x04;
    // Function modes are in bits 4+5
    protected const CONTROL_CLOCK_32768_HZ_MODE = 0x00;
    protected const CONTROL_CLOCK_50_HZ_MODE = 0x10;
    /**
     * In the event-counter mode, events are stored in BCD format.
     * D5 is the most significant and D0 the least significant digit.
     * In combination of all 6 digits, it may count to 1 million.
     */
    protected const CONTROL_EVENT_COUNTER_MODE = 0x20;
    protected const CONTROL_TEST_MODE = 0x30;
    protected const CONTROL_STOP_COUNTING = 0x80;


    /**
     * Counter value.
     * May overflow.
     * @var float
     */
    protected $counting;

    /**
     * Sets as event counter, resets counter registers.
     */
    public function init()
    {
        $this->initAsEventCounter();
        $this->clearEventCounterRegisters();
    }

    public function getName():string
    {
        return "PCF8593";
    }


    public function getAddress():int
    {
        return static::BASE_ADDRESS;
    }

    /**
     * Inits as event counter.
     */
    public function initAsEventCounter()
    {
        $address = static::BASE_ADDRESS + $this->getAddressOffset();
        $i2c = $this->getConnection($address);

        $dataAddress = static::REGISTER_CONTROL_AND_STATUS;
        $value = static::CONTROL_EVENT_COUNTER_MODE;
        $bitmask = base_convert("$value", 10, 2);
        $i2c->set($dataAddress, $value);
    }

    /**
     * Clears those registers that are used as event counter.
     */
    public function clearEventCounterRegisters()
    {
        $this->setEventCounterRegisters(0, 0, 0);
    }

    /**
     * Sets those registers that are used as event counter.
     * Uses BCD mode to write data.
     * @param int $r3 5 and 4
     * @param int $r2 3 and 2
     * @param int $r1 1 and 0
     */
    public function setEventCounterRegisters(int $r3, int $r2, int $r1)
    {
        $address = static::BASE_ADDRESS + $this->getAddressOffset();
        $i2c = $this->getConnection($address);

        $dataAddress = static::REGISTER_BCD_COUNTER_D1_D0;
        $i2c->writeByte($dataAddress, $r1, true);

        $dataAddress = static::REGISTER_BCD_COUNTER_D3_D2;
        $i2c->writeByte($dataAddress, $r2, true);

        $dataAddress = static::REGISTER_BCD_COUNTER_D5_D4;
        $i2c->writeByte($dataAddress, $r3, true);
    }

    /**
     * Clears those registers that are used as RAM.
     */
    public function clearRam()
    {
        $address = static::BASE_ADDRESS + $this->getAddressOffset();
        $i2c = $this->getConnection($address);

        $value = 0;
        $dataAddress = static::REGISTER_RAM_0;
        $i2c->set($dataAddress, $value);
        $dataAddress = static::REGISTER_RAM_1;
        $i2c->set($dataAddress, $value);
        $dataAddress = static::REGISTER_RAM_2;
        $i2c->set($dataAddress, $value);
    }

    /**
     * Reads the counter value from the chip.
     * @return int
     */
    public function getCount(): int
    {
        $address = static::BASE_ADDRESS + $this->getAddressOffset();
        $bus = $this->getConnection($address);

        $int1 = $bus->readByte(static::REGISTER_BCD_COUNTER_D1_D0, true, $raw1); //  i2cget -y 1 81 1 b  -> 0x66
        $int2 = $bus->readByte(static::REGISTER_BCD_COUNTER_D3_D2, true, $raw2);
        $int3 = $bus->readByte(static::REGISTER_BCD_COUNTER_D5_D4, true, $raw3);

        $aData = [
            'raw3' => $raw3,
            'raw2' => $raw2,
            'raw1' => $raw1,

            'int3' => $int3,
            'int2' => $int2,
            'int1' => $int1,
        ];

        $this->debugReadings($aData);

        $this->counting = $int1 + $int2*100 + $int3*100*100;

        return $this->counting;
    }

}
