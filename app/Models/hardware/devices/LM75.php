<?php
/**
 * Every device is connected to a bus and has an address.
 *
 * Address is 0x48 + offset
 *
 *
 * Register:
 *
 * 0x00 - Temperature, 16bit
 *
 * 0x01 - Configuration Register 8bit
 *
 * 0x02 - Hysterisis 16bit
 *
 * 0x03 - Tos 16bit
 *
 * GPL2 Licence
 */
namespace awwa\waschpi\App\Models\hardware\devices;

use awwa\waschpi\App\Models\hardware\I2C;
use awwa\waschpi\App\Models\hardware\OpenSliderI2C;


/**
 * This class represents a LM75
 *
 * It uses a i2c connection
 */
class LM75 extends AbstractI2cDevice implements DeviceInterface
{
    use AddressSelectionTrait;

    protected const BASE_ADDRESS = 0x48;

    protected const TEMP_HIGH_REGISTER = 0;

    protected const REGISTER_TEMPERATURE = 0x00;
    protected const REGISTER_CONFIGURATION = 0x01;
    protected const REGISTER_HYSTERESIS = 0x02;
    protected const REGISTER_OUTPUT_SWITCH = 0x03;

    /**
     * the read value of temperature.
     * @var float
     */
    protected $temperature;


    public function init()
    {
    }

    public function getName():string
    {
        return "LM75";
    }


    public function getAddress():int
    {
        return static::BASE_ADDRESS;
    }

    /**
     * @return float
     */
    public function getTemperature(): float
    {
        $address = static::BASE_ADDRESS + $this->getAddressOffset();
        $bus = $this->getConnection(static::BASE_ADDRESS);
        $raw = $bus->get(static::TEMP_HIGH_REGISTER, OpenSliderI2C::MODE_WORD, true);

//        $value = $raw;
//        $bitmask = base_convert("$value", 10, 2);

        $int = intval($raw);
        $int = $int >> 7;
        $float = $int / 2;
        $this->temperature = $float;

        return $this->temperature;
    }


}
