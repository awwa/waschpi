<?php
/**
 * GPL2 Licence
 */
namespace awwa\waschpi\App\Models\hardware\devices;

use awwa\waschpi\App\Models\hardware\I2C;


/**
 * Defines requirements for a device
 *
 */
interface DeviceInterface
{

    /**
     * Sets the bus number (0 or 1) and address offset.
     *
     * @param int $busNumber
     * @param int $addressOffset
     * @return \self
     */
    public static function create(int $busNumber, int $addressOffset=0);

    /**
     * Creates and returns the i2c connection.
     * @param int $chipAddress
     * @return I2C
     */
    public function getConnection(int $chipAddress): I2C;

    /**
     * Initialises the device.
     */
    public function init();

    /**
     * Gets name of device, e.g. LM75
     * @return string
     */
    public function getName():string;

    /**
     * @return int
     */
    public function getAddress():int;

}