<?php
/**
 * GPL2 Licence
 */
namespace awwa\waschpi\App\Models\hardware\devices;

use awwa\waschpi\lib\I2C;

/**
 * This trait allows to use an address that is combined from a
 * base address and an address offset, e.g. LM75.
 */
trait AddressSelectionTrait
{
    /**
     * the address offset.
     * @var int
     */
    protected $addressOffset = 0;

    public function getAddressOffset(): int
    {
        return $this->addressOffset;
    }

    public function setAddressOffset(int $addressOffset): void
    {
        $this->addressOffset = $addressOffset;
    }

}
