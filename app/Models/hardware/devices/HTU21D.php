<?php
/**
 * Every device is connected to a bus and has an address.
 *
 * The I2C address is: 40h
 *
 * GPL2 Licence
 */

namespace awwa\waschpi\App\Models\hardware\devices;

use awwa\waschpi\lib\I2C;

/**
 * This class represents a HTU21D temperature / humidity meter.
 *
 * It uses a i2c connection
 *
 * measurement command (0xE3 for temperature, 0xE5 for relative humidity),
 *
 * @method HTU21D create(int $busNo) creates chip
 */
class HTU21D extends AbstractI2cDevice implements DeviceInterface
{
    use AddressSelectionTrait;

    protected const BASE_ADDRESS = 0x40;

    // Register Addresses
    // Command Codes
    protected const TRIGGER_TEMPERATURE_MEASUREMENT_HOLD_MASTER = 0xE3; // 227  HOLD MASTER = SCK is blocked and controlled by HTU21D
    protected const TRIGGER_HUMIDITY_MEASUREMENT_HOLD_MASTER = 0xE5; // 229  HOLD MASTER
    protected const TRIGGER_TEMPERATURE_MEASUREMENT_NO_HOLD = 0xF3; // 243  no HOLD
    protected const TRIGGER_HUMIDITY_MEASUREMENT_NO_HOLD = 0xF5; // 245  no HOLD
    protected const WRITE_USER_REGISTER = 0xE6;
    protected const READ_USER_REGISTER = 0xE7;
    protected const SOFT_RESET = 0xFE;

    // Bitmasks User Register
    protected const RESOLUTION_BITS_BITMASK = 0x81;
    protected const RESOLUTION_BITS_BITMASK_INVERTED = 0x7E;
    protected const RESOLUTION_BITMASK_12_14 = 0x00;
    protected const RESOLUTION_BITMASK_8_12 = 0x01;
    protected const RESOLUTION_BITMASK_10_13 = 0x80;
    protected const RESOLUTION_BITMASK_11_11 = 0x81;


    /**
     * Counter value.
     * May overflow.
     * @var float
     */
    protected $counting;

    public function init()
    {
        $this->softReset();
        // wait 20ms
        usleep(20000);
        $this->setResolution();
        usleep(20000);
    }

    public function getName():string
    {
        return "HTU21D";
    }

    public function getAddress():int
    {
        return static::BASE_ADDRESS;
    }

    /**
     * Soft reset.
     * Takes maximum 15ms.
     */
    public function softReset()
    {
        $address = static::BASE_ADDRESS + $this->getAddressOffset();
        $i2c = $this->getConnection($address);
        $value = static::SOFT_RESET;
        $i2c->quickCommand($value);
    } //  i2cset -y 1 64 254 c

    /**
     * Sets resolution to 8/12bit.
     */
    public function setResolution()
    {
        $address = static::BASE_ADDRESS + $this->getAddressOffset();
        $i2c = $this->getConnection($address);
        $command = static::READ_USER_REGISTER;
        $str = $i2c->get($command);
        $int = intval($str);
        // clear bits 7,0 and then set resolution
        $int &= static::RESOLUTION_BITS_BITMASK_INVERTED;
        $int |= static::RESOLUTION_BITMASK_8_12;

        // wait 20ms
        usleep(20000);

        $command = static::WRITE_USER_REGISTER;
        $value = $int;
        $i2c->set($command, $value);
    }  //  i2cset -y 1 64 230 3 b

    /**
     * Reads the desired value from the chip.
     * Longes measuring time is 14ms.
     * @return int
     */
    public function readTemperature(): float
    {
        $address = static::BASE_ADDRESS + $this->getAddressOffset();
        $bus = $this->getConnection($address);

        // trigger measurement
        $command = static::TRIGGER_TEMPERATURE_MEASUREMENT_NO_HOLD;
        $bus->quickCommand($command);
        // wait 20ms
        usleep(20000);

        $str = $bus->getNoAddress();

        $raw = intval($str);

        // this is the official formula:
        $celsius = -46.85 + (175.72 * ($raw / (pow(2,16))));

        // this is found by experiments:
        $slope = 45/72;
        $offset = -37.5;
        $approx = $slope * $raw + $offset;

        $debug = [
            'raw' => $raw,
            'celsius' => $celsius,
            'slope' => $slope,
            'offset' => $offset,
            'approx' => $approx,
        ];

        $this->debugReadings($debug);

        return $approx;
    }
    // trigger: i2cset -y 1 64 243 c
    // read:    i2cget -y 1 64 w


    /**
     * Reads the desired value from the chip.
     * Longes measuring time is 14ms.
     * @return int
     */
    public function readHumidity(): float
    {
        $address = static::BASE_ADDRESS + $this->getAddressOffset();
        $bus = $this->getConnection($address);

        // trigger measurement
        $command = static::TRIGGER_HUMIDITY_MEASUREMENT_NO_HOLD;
        $bus->quickCommand($command);
        // wait 20ms
        usleep(20000);

        $str = $bus->getNoAddress();
        $raw = intval($str);

        // this is the official formula:
        $hum = -6 + (125 * ($raw / pow(2,16)));

        return $raw;
    }
    // trigger: i2cset -y 1 64 243 c
    // read:    i2cget -y 1 64 w

}
