<?php
/**
 * Work in progress
 *
 * MLX90614 requires the "repeated start" condition of I2C to work.
 * On the raspberry pi, this condition is called "combined".
 *
 *
 *
 * GPL2 Licence
 */

namespace awwa\waschpi\App\Models\hardware\devices;

use awwa\waschpi\lib\I2C;

/**
 * This class represents an infrared thermometer.
 *
 * It requires the "repeated start" condition to work.
 *
 * On the raspberry pi, this condition is called "combined".
 *
 * @ignore work in progress
 */
class MLX90614 extends AbstractI2cDevice implements DeviceInterface
{
    use AddressSelectionTrait;

    protected const BASE_ADDRESS = 0xFF;


    // Register Addresses
    protected const RAM_ACCESS_COMMAND_MASK = 0xE0;
    protected const RAM_ACCESS_ADDRESS_MASK = 0x1F;

    public function init()
    {

    }

    public function getName():string
    {
        return "MLX90614";
    }


    public function getAddress():int
    {
        return static::BASE_ADDRESS;
    }


}
