<?php
/**
 * GPL2 Licence
 */
namespace awwa\waschpi\App\Models;

use awwa\waschpi\App\Models\hardware\devices;
use awwa\waschpi\App\Events\WaschpiEvent;
use Illuminate\Support\Facades\Log;


/**
 * Represents a washing machine.
 *
 * Collects data from I2C devices.
 */
class WashingMachine
{

    private static $simulationFile = 'wmsimulation.tsv';
    private $busNumber = 1;

    /**
     * Returns a WaschpiEvent object.
     * @param WaschpiEvent $PreviousEvent Optional. Is used to calculate rpm.
     * @return WaschpiEvent|null
     */
    public function getData(WaschpiEvent $PreviousEvent=null):?WaschpiEvent
    {
        $simulationFile = self::getSimulationDataFile();
        if ($simulationFile && is_readable($simulationFile)) {
            return $this->getSimulatedData($simulationFile);
        }
        else {
            return $this->getBusData($PreviousEvent);
        }
    }


    public static function getSimulationDataFile():string
    {
        $storeFolder = env('APP_STORE');
        $simulationFile = dirname(__FILE__,3). "/$storeFolder/".self::$simulationFile;
        // $simulationFile = realpath($simulationFile);
        return $simulationFile;
    }

    public static function getSimulationDataFileName():string
    {
        return self::$simulationFile;
    }

    /**
     * Returns a WaschpiEvent of a simulated washCycle.
     * @param string $filePath
     * @param WaschpiEvent $PreviousEvent Optional. Is used to calculate rpm.
     * @return WaschpiEvent|null
     */
    public function getSimulatedData(string $filePath, WaschpiEvent $PreviousEvent=null):?WaschpiEvent
    {
        // get file contents and shift off first line
        $arr = file($filePath);
        $strLine = array_shift($arr);

        if ($strLine === null) {
            $Event = new WaschpiEvent();
        }
        else {
            // create event object
            $aLine = str_getcsv($strLine, "\t", " ");
            $Event = WaschpiEvent::fromIndexedArray($aLine);

            // write back reduced data
            $content = implode(PHP_EOL, $arr);
            $written = file_put_contents($filePath, $content);
            if($written === false) {
                throw new \Exception("unable to write back reduced simulation data");
            }
        }


        return $Event;
    }


    /**
     * Reads sensor values and returns them as WaschpiEvent object.
     * @param WaschpiEvent $PreviousEvent Optional. Is used to calculate rpm.
     * @return WaschpiEvent|null
     */
    public function getBusData(WaschpiEvent $PreviousEvent=null):?WaschpiEvent
    {
        try {
            $temperatureControl = null;
            $LM75 = new devices\LM75($this->busNumber);
            $temperatureControl = $LM75->getTemperature();
        }
        catch (\Throwable $exc) {
            $temperatureControl = null;
            Log::notice("LM75 failed: ".$exc->getMessage());
        }

        try {
            $temperatureDrum = null;
            $humidityDrum = null;
            $HTU21 = new devices\HTU21D($this->busNumber);
            $temperatureDrum = $HTU21->readTemperature();
            $humidityDrum = $HTU21->readHumidity();
        }
        catch (\Exception $exc) {
            $temperatureDrum = 0;
            $humidityDrum = 0;
            Log::notice("try HTU21 init because: ".$exc->getMessage());
            try {
                $HTU21->init();
            }
            catch (\Exception $exc) {
                Log::notice("HTU21 init failed: ".$exc->getMessage());
            }
        }

        try {
            $drumCount = null;
            $PCF8593 = devices\PCF8593::create($this->busNumber);
            $PCF8593->initAsEventCounter();
            $drumCount = $PCF8593->getCount();
        }
        catch (\Throwable $exc) {
            $PCF8593->init();
            $drumCount = 0;
            Log::notice("PCF8593 initialized due to ".$exc->getMessage());
        }

        Log::debug("collecting sensor data complete");

        // calculate RPM - revolutions per minute
        $rpm = 0;
        $Event = new WaschpiEvent();
        $Event->setAbsoluteDrumCount($drumCount);
        $Event->setControlBoxTemperature($temperatureControl);
        $Event->setDrumTemperature($temperatureDrum);
        $Event->setDrumHumidity($humidityDrum);

        // calculate RPM - revolutions per minute
        if ($PreviousEvent !== null) {
            $previousDrumCount = $PreviousEvent->getAbsoluteDrumCount();
            $previousTimestamp = $PreviousEvent->getTimestamp();
            $currentTimestamp = $Event->getTimestamp();
            $timeDiff = $currentTimestamp - $previousTimestamp;
            // in case events have been skipped, this formula calculates the rpm correctly anyway:
            $rpm = intval(floor(($drumCount - $previousDrumCount) / ($timeDiff / 60) ));
            $Event->setRpm($rpm);
        }

        $aData = $Event->toArray();
        Log::debug("creating event object complete [".count($aData)."]", $aData);

        return $Event;
    }

}
