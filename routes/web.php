<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// Frontend
$router->get('/', function () use ($router) {
    Log::debug("requesting root");
    $aVars =  [
        'listurl' => 'events',
        'statusurl' => 'status',
        'projectname' => 'Waschpi',
    ];
    return view('frontend', $aVars);
});

$router->get('/lib/numberblock', function () {
    $view = view('lib.numberblock-js');
    $response = response($view, 200)
                ->header('Content-Type', 'application/javascript');
    return $response;
});


// API
// ------------------------------------------------
$router->get('version', function () use ($router) {
    return $router->app->version();
});

// api help
$router->get('help', function() use ($router){
    $help = [
        'eventLogger' => "triggers an event. This fetches data and stores it",
        'events' => " returns a list of current measurements",
        'event/{cycleid}' => "returns archived measurements (@todo)",
        'status' => "calculates the grand total",
        'current' => "Shows current measurements. Does not add to store.",
        'simulation/{starttime}/{runtime}/{cyclelength}' => "Writes simulation data to the store.",
    ];
    return $help;
});


// web path to trigger a measurement
$router->get('eventLogger', ['uses' => 'CronController@logEvent', 'as' => 'cronMinute']);

// returns the current measurements
$router->get('events', ['uses' => 'EventController@getList', 'as' => 'eventList']);

// returns archived measurements (@todo)
$router->get('event/{cycleid}', ['uses' => 'EventController@getArchivedMeasurements']);

// calculates the grand total
$router->get('status', ['uses' => 'StatusController@findBestMatch']);

// makes an immediate measurement
$router->get('current', ['uses' => 'EventController@getCurrentMeasurements']);

// $router->get('simulation/{starttime}/{runtime}/{cyclelength}',
$router->get('simulation',
    ['uses' => 'SimulationController@writeSimulation']);




