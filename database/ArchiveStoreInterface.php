<?php
/**
 * GPL2 Licence
 */
namespace awwa\waschpi\Database;

use awwa\waschpi\App\Events\WaschpiEvent;
use awwa\waschpi\App\Models\Washcycles\WashCycle;


/**
 * Defines a archive store that holds a past wash cycle.
 *
 * @todo Must be refactored
 */
interface ArchiveStoreInterface
{

    public function setStorePath(string $path);

    public function setTimestamp(int $timestamp);

    /**
     * Stores cycle to database.
     * @param null|WashCycle $Cycle May be null
     * @return ?int Total count of cycles
     * @throws \Exception
     */
    public function setWashCycle(?WashCycle $Cycle):?int;


    public function getStatus(): array;

    /**
     *
     * @param int $limit
     * @return WashCycle[]
     */
    public function getCycles(int $limit=10);

    /**
     * Gets the wash cycle which is identified by the index.
     * Index counts from most recent on: 0 -> newest.
     *
     * @param int $index
     * @return ?WashCycle
     */
    public function getCycle(int $index) : ?WashCycle;


    /**
     * Returns the match data of the given washcycle.
     * @param WashCycle
     * @return array[]
     */
    public function getMatchFromCycle($WashCycle):?array;

    /**
     * @todo this should be moved to the user interface
     *
     * Returns list of good matches of washcycles.
     *
     * The list is sorted best match at first.
     * @param WaschpiEvent[] $aCurrentEvents The events of the running wash cycle. Must be oldest first
     * @param int $count
     * @return array[]
     */
    public function findBestMatches($aCurrentEvents, int $count=10):?array;

    /**
     * Cleans outdated and invalid data.
     * @param int $age Clean by age of document in seconds
     * @param int $limit  Limit total amount of documents (max 1000)
     * @param bool $dryrun True (default) to only show what would be done
     * @return array
     * @throws \Exception
     */
    public function cleanExcessiveData(int $age=0, int $limit=0, bool $dryrun=true);


}
