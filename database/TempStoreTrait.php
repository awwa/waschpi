<?php
/**
 * GPL2 Licence
 */
declare(strict_types=1);
namespace awwa\waschpi\Database;

use awwa\waschpi\devices;


/**
 * Enables a class to hold and process temporary waschpi events
 */
trait TempStoreTrait
{

    /**
     * (30) Threshold for the sum of drum revolutions to be detected as a valid wash cycle.
     * The drum count sum is reset whenever the defined count of subsequent idle
     * events are detected.
     * @var int
     */
    protected $drumRevolveSumThreshold = 30;
    /**
     * (2) Rpm must exceed this threshold to be counted as valid event.
     * Else it is an idle event.
     * @var int
     */
    protected $rpmNoiseThreshold = 2;
    /**
     * (4) Count of idle events to trigger wash cycle harvesting.
     * @var int
     */
    protected $maxIdleEventsInWashCycle = 4;
    /**
     * (180) It is assumed that no washcycle run longer than this value.
     * @var int
     */
    protected $maxWashCycleDuration = 180;



    /**
     * Removes rpm-zero events from start and tail of a wash cycle.
     * Does not reverse order of cycles.
     * @param array $aAllEvents
     * @return array
     */
    protected function trimEmptyEvents(array $aAllEvents)
    {
        // strip empty events at beginning
        foreach ($aAllEvents as $i => $Event) {
            $rpm = $Event->getRpm();
            if ($rpm === 0) {
                unset($aAllEvents[$i]);
            }
            else {
                break;
            }
        }

        $aAllEvents = array_values($aAllEvents);

        // strip empty event at start
        $aAllEvents = array_reverse($aAllEvents);
        foreach ($aAllEvents as $i => $Event) {
            $rpm = $Event->getRpm();
            if ($rpm === 0) {
                unset($aAllEvents[$i]);
            }
            else {
                break;
            }
        }

        $aAllEvents = array_values($aAllEvents);
        $aAllEvents = array_reverse($aAllEvents);

        return $aAllEvents;
    }


    /**
     * Gets current washcycle status and current events.
     *
     * Returns count of idle and running minutes.
     * Only the last 180min are evaluated.
     * The machine is idle if the current event is rpm=0 or
     * the sum of rpm in the last 3 mins is <=3
     *
     * @return array 'currentEvents' Order of current events is newest first
     * @return int 'elapsed' Time since the last state change
     * @return string 'startState' The state, the system has now, can be "running" or "idle"
     * @return string 'currentState' The state before the last state change, can be "running" or "idle"
     */
    public function getCurrentWashCycleStatus()
    {
        $idleMinutes = 0;
        $runningMinutes = 0;
        $segment = 180;
        $aDebugRpmSum = [];

        // sort newest first
        $aAllEventsRaw = $this->getAllEvents($segment);
        $aAllEvents = array_values($aAllEventsRaw);
        $aCurrentEvents = [];

        // Function to calculate state from average rpm
        $getState = function ($rpmSum) {
            // threshold is 6
            if ($rpmSum >= 6) {
                $state = 'running';
            }
            else {
                $state = 'idle';
            }
            return $state;
        };

        // use the first 3 mins to detect initial state of the time segment
        // try to guess status and find start or end.
        $rpmSumArray = [];
        $rpmSumLength = 3;
        $isRunning = null;
        $isIdle = null;
        $startState = null;
        $currentState = null;
        $currentStateSince = 0;
        $elapsedMinutes = 0;
        // $aAllEvents is newest first, the loop walks towards older entries
        for ($i = 0; $i < $segment; $i++) {
            // runs from 0 to 179 = count 180
            /** @var WaschpiEvent $Event */
            $Event = $aAllEvents[$i]??null;
            if ($Event === null) {
                // end of data
                break;
            }
            $aCurrentEvents[] = $Event;

            // calculate rpm sum
            $rpm = $Event->getRpm();
            $rpmSumArray[] = $rpm;
            if (count($rpmSumArray) > $rpmSumLength) {
                array_shift($rpmSumArray);
            }
            $rpmSum = array_sum($rpmSumArray);
            $aDebugRpmSum[] = intval($rpmSum);
            if ($i === $rpmSumLength) {
                // set start state
                $startState = $getState($rpmSum);
            }

            if ($startState !== null) {
                $elapsedMinutes++;
                // detect state change after start state set
                $currentState = $getState($rpmSum);
                // detect state change
                if ($currentState !== $startState) {
                    break;
                }
            }

        }

        return [
            'rawRpmSum' => $aDebugRpmSum,
            'currentEvents' => $aCurrentEvents,
            'elapsed' => $elapsedMinutes,
            'startState' => $startState,
            'currentState' => $currentState,
        ];
    }



}
