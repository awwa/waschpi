<?php
/**
 *
 * GPL2 Licence
 */
namespace awwa\waschpi\Database;

use awwa\waschpi\App\Events\WaschpiEvent;
use awwa\waschpi\App\Models\Washcycles\WashCycle;
use awwa\waschpi\App\Models\Washcycles\WashcycleFormatter;
use Exception;
use awwa\waschpi\Database\TupelStore;
use Illuminate\Support\Facades\Log;
use awwa\waschpi\App\Providers\TimeTrait;


/**
 * Represents all archived wash cycles.
 * @todo
 */
class ArchiveManager
//implements ArchiveStoreInterface, StoreInterface
{
    use TimeTrait;

    protected $timestamp;


    protected $storePath;

    /**
     * Name of archive files.
     */
    protected $filename = 'archive';

    /**
     * Is a file pointer
     */
    protected $metaFile;

    protected $metaData;

    protected $FileStore;


    public static function loadFromTimeDate(string $isoTimeDate)
    {
    }

    public static function loadFromFile(string $filePath)
    {
        $Self = new ArchiveStore();
        $FileStore = TupelStore::loadFromFile($filePath);
        $Self->setDataStore($FileStore);


    }


    public function save()
    {
        $DataStore = $this->getDataStore();
        $filename = $this->getFilename();
        $DataStore->setFilepath($filename, $this->storePath);
        $DataStore->save();
        $this->saveMetadata();
    }


    public function setDataStore(TupelStore $DataStore)
    {
        $this->FileStore = $DataStore;
    }


    public function getDataStore():?TupelStore
    {
        if (empty($this->FileStore)) {
            $filename = $this->getFilename();
            if (empty($filename)) {
                // without filename, no file can exist
                $this->FileStore = null;
            }
            else {
                $this->FileStore = new TupelStore();
                $filename = $this->getFilename();
                if (empty($filename)) {
                    $this->FileStore->setFilepath($filename, $this->storePath);
                }
            }
        }
        return $this->FileStore;
    }


    public function setStorePath(string $path):void
    {
        $this->storePath = $path;
    }


    public function getStorePath():?string
    {
        return $this->storePath;
    }


    public function setTimestamp(int $timestamp):void
    {
        $this->timestamp = $timestamp;
        Log::debug(__METHOD__."(): ".$this->timestamp);
        $FileStore = $this->getDataStore();
        $FileStore->setFilepath($this->getFilename(), $this->storePath);
    }


    public function getTimestamp():?int
    {
        if(!$this->timestamp) {
            // $this->timestamp = time(); // time is always set by intention
            // throw new Exception("timestamp is required");
            return null;
        }
        $timestamp = $this->timestamp;
        return $timestamp;
    }


    public function setMetadata($metadata)
    {
        $this->metaData = $metadata;
    }

    public function saveMetadata()
    {
        $metadata = $this->metaData;
        Log::debug(__METHOD__."(): ".$this->timestamp);
        $this->timestamp = $metadata['timestamp'] ?? $this->timestamp;
        if(!$this->timestamp) {
            // throw new Exception("timestamp is required");
            $this->timestamp = null;
        }
        $filepath = $this->getFilepath();
        Log::debug(__METHOD__."(): writing metadata to ".$filepath);
        file_put_contents($filepath.'.json',json_encode($metadata));
    }


    public function getMetadata()
    {
        return $this->metaData;
    }


    public function loadMetadata()
    {
        Log::debug(__METHOD__."(): ".$this->timestamp);
        $this->timestamp = $metadata['timestamp'] ?? $this->timestamp;
        if(!$this->timestamp) {
            // throw new Exception("timestamp is required");
            $this->timestamp = null;
        }
        $filepath = $this->getFilepath();
        Log::debug(__METHOD__."(): getting metadata from ".$filepath);
        $json = file_get_contents($filepath.'.json');
        $this->metaData = json_decode($json);
        return $this->metaData;
    }

    /**
     * Clears metadata except timestamp
     */
    public function clearMetadata():void
    {
        // $initial = ['timestamp'=>$this->timestamp];
        $initial = [];
        $this->setMetadata($initial);
    }

    /**
     * Drops metadata store
     * @todo this could be replaced by meta data object
     */
    public function dropMetadata():void
    {
        $this->metaData = [];
        $filepath = $this->getFilepath();
        Log::debug(__METHOD__."(): ".$filepath);
        if($filepath) {
            $fullFilePathName = $filepath.'.json';
            if(file_exists($fullFilePathName)) {
                $success = \unlink($fullFilePathName);
                if($success === false) {
                    throw new \Exception("dropping metadata file failed ".$fullFilePathName);
                }
            }
        }
    }

    /**
     * Gets filepath from internal path and timestamp
     */
    public function getFilepath()
    {
        if(!$this->storePath) {
            throw new Exception("storePath is required");
        }
        $path = $this->storePath.'/'.$this->getFilename();
        Log::debug(__METHOD__."(): calculated filepath to ".$path);
        return $path;
    }


    public function getFilename()
    {
        $timestamp = $this->getTimestamp();
        if(!$timestamp) {
            return null;
            // throw new Exception("timestamp is required");
        }
        return $this->getFilenameFromTimestamp($timestamp);
    }


    protected function getFilenameFromTimestamp(int $timestamp)
    {
        $isoString = date("Y-m-d\THi", $timestamp);
        Log::debug(__METHOD__."(): iso string ".$isoString);
        // remove timezone
        $isoString = substr($isoString, 0, 19);  // 2004-02-12T15:19:21+00:00
        return $isoString.$this->filename;
    }


    public function getStatus(): array
    {
        throw new Exception("todo");
    }

    /**
     * Adds wash cycle to store.
     * Meta info and data series are stored in separate files.
     * @param null|WashCycle $Cycle May be null
     * @return ?int Total count of cycles
     * @throws \Exception
     */
    public function addWashCycle(?WashCycle $Cycle):?int
    {
        if ($Cycle === null) {
            return null;
        }

        $metaInfo = $Cycle->toArrayMetaInfo();
        $this->setMetadata($metaInfo);

        $eventSeries = $Cycle->toArrayEventSeries();
        $FileStore = $this->getDataStore();
        foreach($eventSeries as $dataset) {
            $FileStore->addDataset($dataset);
        }

        $totalCount = count($eventSeries);

        return $totalCount;
    }

    /**
     * @deprecated in a file store this should work different now: e.g. fetch cycles by glob()
     * @param int $limit
     * @return WashCycle[]
     */
    public function getCycles(int $limit=10)
    {
        $FileStore = $this->getDataStore();
        $aData = $FileStore->getAllDatasets($limit);
        return $aData;
    }

    /**
     * Gets the wash cycle which is identified by the index.
     * Index counts from most recent on: 0 -> newest.
     *
     * @deprecated in a file store this should work different now
     *
     * @param int $index
     * @return ?WashCycle
     */
    public function getCycle(int $index) : ?WashCycle
    {
        $collection = $this->getCollection();
        // find last entries

        $options = [
            'sort' => [
                'timestamp' => self::TIMESTAMP_NEWEST_FIRST,
            ],
            'skip' => $index,
            'limit' => 1,
        ];
        $cursor = $collection->find([], $options);

        $WashCycle = null;
        if ($cursor) {
            $aData = $cursor->toArray();
            foreach ($aData as $oDoc) {
                $aEntry = $oDoc->getArrayCopy();
                $WashCycle = WashCycle::fromArray($aEntry);
            }
        }

        return $WashCycle;
    }

    /**
     * Returns the match data of the given washcycle.
     * @param WashCycle
     * @return array[]
     */
    public function getMatchFromCycle($WashCycle):?array
    {
        $aCycleEvents = $WashCycle->getEventSeries();
        $aResult['order'] = WashCycle::getSortOrder($aCycleEvents);

        $TempTable = app()->make(TempStore::class);
        $aCurrentOldestFirst = $TempTable->getAllEvents(null, TempTable::TIMESTAMP_OLDEST_FIRST);

        /** @var WashCycle $WashCycle */
        $aCompareResult = $WashCycle->compareSerieWithSweep($aCurrentOldestFirst);
        $aResult['smallestDelta'] = $aCompareResult['smallestDelta'];
        $aResult['smallestDeltaOffset'] = $aCompareResult['smallestDeltaOffset'];
        // maybe unused:
        $Formatter = new WashcycleFormatter($WashCycle);
        $aResult['metadata'] = $Formatter->toDebugOutput();

        return $aResult;
    }

    /**
     * Returns list of good matches of washcycles.
     *
     * The list is sorted best match at first.
     * @param WaschpiEvent[] $aCurrentEvents The events of the running wash cycle. Must be oldest first
     * @param int $count
     * @return array[]
     */
    public function findBestMatches($aCurrentEvents, int $count=10):?array
    {
        // loop all stores cycles and match them with the given events
        $aCycles = $this->getCycles($count);
        $aResults = [];

        $BestMatchingCycle = null;
        foreach ($aCycles as $key => $Cycle) {
            $aResult = [];
            try {
                $aCycleEvents = $Cycle->getEventSeries();
                $aResult['order'] = WashCycle::getSortOrder($aCycleEvents);

                /** @var WashCycle $Cycle */
                $aCompareResult = $Cycle->compareSerieWithSweep($aCurrentEvents);
                $aResult['smallestDelta'] = $aCompareResult['smallestDelta'];
                $aResult['smallestDeltaOffset'] = $aCompareResult['smallestDeltaOffset'];
                $Formatter = new WashcycleFormatter($Cycle);
                $aResult['metadata'] = $Formatter->toDebugOutput();
            }
            catch (\Exception $exc) {
                $aResult['error'] = $exc->getMessage();
            }

            $aResults[] = $aResult;
        }

        // sort best matches (lowest deltasum) at beginning of array
        usort($aResults, function($a, $b) {
            if (!isset($a['smallestDelta'])) {
                return 0;
            }
            if (!isset($b['smallestDelta'])) {
                return 0;
            }
            if ($a['smallestDelta'] == $b['smallestDelta']) {
                return 0;
            }
            return ($a['smallestDelta'] < $b['smallestDelta']) ? -1 : 1;
        });

        return $aResults;
    }


    /**
     * Cleans outdated and invalid data.
     * @param int $age Clean by age of document in seconds
     * @param int $limit  Limit total amount of documents (max 1000)
     * @param bool $dryrun True (default) to only show what would be done
     * @return array
     * @throws \Exception
     */
    public function cleanExcessiveData(int $age=0, int $limit=0, bool $dryrun=true)
    {
        // get all events
        $collection = $this->getCollection();
        $filter = [];
        $options = [
            'sort' => [
                'timestamp' => self::TIMESTAMP_NEWEST_FIRST,
            ],
        ];

        $options['limit'] = 1000;
        if ($limit > $options['limit']) {
            throw new \Exception("limit must not be greater than ".$options['limit']);
        }

        $cursor = $collection->find($filter, $options);

        // list datasets and add to delete list
        $aAllEvents = [];
        $i = 1;
        $aDelete = [];
        foreach ($cursor->toArray() as $oDoc) {
            $aEntry = $oDoc->getArrayCopy();
            /** @var WashCycle $Cycle */
            $Cycle = WashCycle::fromArray($aEntry);
            $identifier = $Cycle->getDbId();
            $eventCount = $Cycle->getEventCount();
            $timestamp = $Cycle->getTimestamp();
            $ageOfEntry = time() - $timestamp;

            // limit length to max cycle time
            if($limit && $i > $limit) {
                $aDelete[] = $timestamp;
            }
            elseif ($age && $ageOfEntry > $age) {
                $aDelete[] = $timestamp;
            }
            elseif ($eventCount < 10) {
                // delete unsufficient cycles
                $aDelete[] = $timestamp;
            }
            $i++;
        }

        // delete surplus wash cycles
        $deletedCount = 0;
        $ackCount = 0;
        // it should be noted here, that raspberrypi's mongodb appears
        // not to work with mongo object ids
        foreach ($aDelete as $identifier) {
            /** @var \MongoDB\DeleteResult $result */
            if(!$dryrun) {
                $result = $collection->deleteOne(['timestamp'=>$identifier]);
                $ackCount += $result->isAcknowledged()?1:0;
                $deletedCount += $result->getDeletedCount();
            }
        }

        if($dryrun) {
            $strDryRun = "Dryrun deleted";
        }
        else {
            $strDryRun = "Deleted";
        }
        Config::get()->getErrorLog()->notice("$strDryRun [$deletedCount] of ". count($aDelete)." of ".$i);

        return [
            'total' => $i,
            'toDelete' => count($aDelete),
            'acknowledged' => $ackCount,
            'deleted' => $deletedCount,
        ];
    }


    public function clearStore()
    {
        $FileStore = $this->getDataStore();
        if ($FileStore) {
            $FileStore->clearStore();
        }
        $this->clearMetadata();
    }


    public function dropStore()
    {
        $FileStore = $this->getDataStore();
        $FileStore->dropStore();
        $this->dropMetadata();
    }

    // public function __destruct()
    // {
    //     $this->setMetadata_to_file();
    //     throw new Exception("make manifest for file store is todo");
    // }

}
