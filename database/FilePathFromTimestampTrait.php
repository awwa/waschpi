<?php
/**
 * Provides timestamp to filename functions.
 * GPL2 Licence
 */
namespace awwa\waschpi\Database;

use Illuminate\Support\Facades\Log;



/**
 *
 */
trait FilePathFromTimestampTrait
{
    /**
     * Stores wash events
     */
    // protected $fileStore;

    protected $timestamp;


    /**
     * Sets timestamp, calculates filename and sets the filename.
     */
    public function setTimestamp(int $timestamp):void
    {
        $this->timestamp = $timestamp;
        $filePath = $this->getFilepath();
        Log::debug(__METHOD__."(): ".$this->timestamp. " => ".$filePath);
        $Store = $this->getStore();
        $Store->setFilepath($filePath);
    }


    public function getTimestamp():?int
    {
        // time is always set by intention on this level
        if(!$this->timestamp) {
            throw new \Exception("timestamp is required");
            // return null;
        }
        $timestamp = $this->timestamp;
        return $timestamp;
    }


    /**
     * Calculates filepath from timestamp
     */
    public function getFilenameFromTimestamp(int $timestamp)
    {
        $isoString = date("Y-m-d\THi", $timestamp);
        Log::debug(__METHOD__."(): iso string ".$isoString);
        // remove timezone
        $isoString = substr($isoString, 0, 19);  // 2004-02-12T15:19:21+00:00
        return $isoString.$this->filename;
    }

}
