<?php
/**
 * A file store holds its data in tab separated lines.
 * Each line is a dataset.
 * GPL2 Licence
 */
declare(strict_types=1);

namespace awwa\waschpi\Database;

use Illuminate\Support\Facades\Log;



/**
 * Holds vars and methods to access a file
 */
trait FileStoreTrait
{

    protected static $fileHandle;
    protected static $separator = "\t";
    protected static $enclosure = ' ';
    protected static $escapeChar = "\\";

    /**
     * Name of archive files.
     */
    protected $filepath = '';

    public function setFilepath($filepath, $path=null)
    {
        if($path !== null) {
            $filepath = $path.'/'.$filepath;
        }
        $this->filepath = $filepath;
    }

    public function getFilepath()
    {
        if(!$this->filepath) {
            // throw new \Exception("filename empty");
            Log::warning(__METHOD__. "(): filename was empty");
            return null;
        }
        else {
            $pathParts = \pathinfo($this->filepath);
            $realFilepath = $pathParts['dirname'] .'/'. $pathParts['filename'].'.'.$this->getFileExtension();
            $this->filepath = $realFilepath;
            return $this->filepath;
        }
    }

}
