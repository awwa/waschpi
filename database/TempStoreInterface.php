<?php
/**
 * GPL2 Licence
 *
 */

namespace awwa\waschpi\Database;

use awwa\waschpi\App\Models\Washcycles\WashCycle;
use awwa\waschpi\App\Events\WaschpiEvent;
use awwa\waschpi\Database\StorableInterface;


/**
 * Describes the store for current waschpi events.
 */
interface TempStoreInterface
{
    public const TIMESTAMP_NEWEST_FIRST = -1;
    public const TIMESTAMP_OLDEST_FIRST = 1;


    /**
     * Gets all events.
     * @param int $limit
     * @param int $sort
     * @return WaschpiEvent[] Default is newest first
     */
    public function getAllEvents(int $limit=null, int $sort=self::TIMESTAMP_NEWEST_FIRST);

    /**
     * Gets last complete wash cycle and clears the temp table.
     *
     * If the wash cycle is not complete, it is not fetched and the temp table is left untouched.
     *
     * If more than one cycle is detected, only the last is evaluated.
     *
     * The cycle is returned with oldest event first.
     *
     * 1) Detect running wash cycle: a pause longer than 5min is considered as idle state.
     * 2) Harvest all the table and scan for a wash cycle.
     * 3) Strips rpm-zero events
     *
     * @param array $stats Returns debug info
     * @param bool $cleanTable If false, the temp table is not emptied after harvesting.
     * @return WashCycle|null
     */
    public function harvestLastWashCyle(array &$stats=null, $cleanTable=true):?WashCycle;

    /**
     *
     * @param int $referenceTime
     * @return WaschpiEvent|null
     * @throws \Exception
     */
    public function getLastEvent(int $referenceTime=null): ?WaschpiEvent;

    /**
     * Gets current washcycle status and current events.
     *
     * Returns count of idle and running minutes.
     * Only the last 180min are evaluated.
     * The machine is idle if the current event is rpm=0 or
     * the sum of rpm in the last 3 mins is <=3
     *
     * @return array 'currentEvents' Order of current events is newest first
     * @return int 'elapsed' Time since the last state change
     * @return string 'startState' The state, the system has now, can be "running" or "idle"
     * @return string 'currentState' The state before the last state change, can be "running" or "idle"
     */
    public function getCurrentWashCycleStatus();

    /**
     * Cleans data independent of contained data.
     * @param int $age Clean by age of document in seconds
     * @param int $limit  Limit total amount of documents (max 1000)
     * @param bool $dryrun True (default) to only show what would be done
     * @return array
     * @throws Exception
     */
    public function cleanExcessiveData(int $age=0, int $limit=0, bool $dryrun=true);

    /**
     * Stores an event in the collection.
     * @param StorableInterface $Event
     * @return \MongoDB\InsertOneResult
     * @throws \Exception
     */
    public function addEvent(StorableInterface $Event);


    public function clearStore();

}
