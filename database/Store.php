<?php
/**
 * A wash cycle consists of events.
 * Events contain temperatures and rotation counts.
 *
 * GPL2 Licence
 */
declare(strict_types=1);

namespace awwa\waschpi\Database;

use awwa\waschpi\Database\StorableInterface;
use Exception;
use Illuminate\Support\Facades\Log;
use awwa\waschpi\Database\TupelStore;


/**
 * Holds data and metadata file and the keys.
 * Bears knowledge about fields and types.
 * TupelStore knows how to apply fields and types.
 */
class Store
{
    public const KEY_META_FIELDS = 'metaKeyFields';
    /**
     * Path to store folder
     * @deprecated
     */
    protected static $storePath;

    protected $filepath;

    /** @var TupelStore */
    protected $FileStore;
    /** @var JsonDataStore */
    protected $MetaData;


    public function __construct()
    {
        $this->MetaData = new JsonDataStore();
        $this->FileStore = new TupelStore();
    }


    public function setDataFields(array $metaFields)
    {
        $this->setMetaItem(self::KEY_META_FIELDS, $metaFields);
        $this->FileStore->setDataFields($metaFields);
    }


    public function setFilepath($filepath, $path=null)
    {
        if($path !== null) {
            $filepath = $path.'/'.$filepath;
        }
        $this->filepath = $filepath;
        $this->MetaData->setFilepath($filepath);
        $this->FileStore->setFilepath($filepath);
    }

    /**
     * Sets a key in meta info.
     */
    public function setMetaItem(string $name, $data)
    {
        $this->MetaData->set($name, $data);
    }

    public function getMetaItem(string $name)
    {
        return $this->MetaData->get($name);
    }

    public function setMetaDataArray(array $array)
    {
        $this->MetaData->setMetadata($array);
    }

    public function setMetaObject(JsonDataStore $MetaData)
    {
        $this->MetaData = $MetaData;
    }

    public function getMetaObject():JsonDataStore
    {
        return $this->MetaData;
    }


    public function setFileStore(TupelStore $Store)
    {
        $this->FileStore = $Store;
    }

    public function getFileStore():TupelStore
    {
        return $this->FileStore;
    }

    /**
     * Loads store data from tupel store and adds keys from meta file.
     */
    public static function loadFromFile(string $filePathAndName, array $fields=null):Store
    {
        $fullFilePath = $filePathAndName;
        $Self = new self();
        // 1) load meta
        $MetaData = JsonDataStore::loadFromFile($fullFilePath);
        if ($fields === null) {
            $fields = $MetaData->get(self::KEY_META_FIELDS);
        }
        else {
            $MetaData->set(self::KEY_META_FIELDS, $fields);
        }
        $Self->setMetaObject($MetaData);

        // 2) load data file
        $FileStore = TupelStore::loadFromFile($fullFilePath, $fields);

        $Self->setFileStore($FileStore);
        return $Self;
    }

    /**
     * Stores arrays.
     */
    public function addDataset(array $array):array
    {
        $result = $this->FileStore->addDataset($array);
        return $result;
    }

    /**
     * Stores an event in the collection.
     */
    public function addStorable(StorableInterface $Storable)
    {
        if($Storable instanceof StorableInterface) {
            $fields = $Storable->toArray();
        }
        else {
            throw new \Exception(__METHOD__."(): data is not storable");
        }
        return $this->addDataset($fields);
    }


    public function getStoreCount()
    {
        $array = $this->FileStore->getAllDatasets();
        return count($array);
    }

    /**
     * Gets all events.
     * Events in the store are ordered newest last.
     */
    public function getAllDatasets($limit=null)
    {
        $datasets = $this->FileStore->getAllDatasets($limit);
        return $datasets;
    }


    public function getLastDataset(): ?array
    {
        $dataset = $this->FileStore->getLastDataset();
        return $dataset;
    }

    public function truncate($size)
    {
        $this->FileStore->truncate($size);
    }

    /**
     * Stores the data to file.
     */
    public function save()
    {
        $this->MetaData->saveMetadata();
        $this->FileStore->save();
    }

    public function clearStore()
    {
        $this->MetaData->onDataClear();
        $this->FileStore->clearStore();
    }


    public function dropStore()
    {
        $this->MetaData->dropMetadata();
        $this->FileStore->dropStore();
    }

}
