<?php
/**
 *
 * GPL2 Licence
 */
namespace awwa\waschpi\Database;

use awwa\waschpi\Database\FileStoreTrait;
use Illuminate\Support\Facades\Log;


/**
 * Holds json metadata file management.
 */
class JsonDataStore implements FileStoreInterface
{
    use FileStoreTrait;

    protected $metaData = [];


    public function getFileExtension()
    {
        return 'json';
    }

    public function setMetadata(array $metadata)
    {
        $this->metaData = $metadata;
    }


    public function set(string $name, $data)
    {
        $this->metaData[$name] = $data;
    }

    public function get(string $name)
    {
        return $this->metaData[$name]??null;
    }

    public function saveMetadata()
    {
        $metadata = $this->metaData;
        // Log::debug(__METHOD__."(): ".$this->timestamp);
        $filepath = $this->getFilepath();
        Log::debug(__METHOD__."(): writing metadata to ".$filepath);
        $json = json_encode($metadata, JSON_PRETTY_PRINT);
        $handle = fopen($filepath, 'w');
        $bytes = fputs($handle, $json);
        if($bytes === false){
            throw new \Exception("json meta data save failed");
        }
    }


    public function getMetadata()
    {
        return $this->metaData;
    }


    public static function loadFromFile(string $filepath)
    {
        Log::debug(__METHOD__."(): getting metadata from ".$filepath);
        $MetaData = new self();
        $MetaData->setFilepath($filepath);
        $newFilepath = $MetaData->getFilepath();
        $handle = fopen($newFilepath, 'c+');
        if($handle === false) {
            throw new \Exception("metadata filehandle failed ".$newFilepath);
        }

        $json = "";
        if ($handle) {
            while (($buffer = fgets($handle, 4096)) !== false) {
                $json .= $buffer;
            }
            if (!feof($handle)) {
                throw new \Exception(__METHOD__. "(): metadata fgets file failed ".$newFilepath);
            }
            fclose($handle);
        }

        if(empty($json)) {
            $json = '[]';
            $bytes = file_put_contents($newFilepath, $json);
            if($bytes === false) {
                throw new \Exception("load metadata file failed ".$newFilepath);
            }
        }
        $meta = json_decode($json, true);
        $MetaData->setMetadata($meta);

        return $MetaData;
    }

    /**
     * Clear actions on date file clear.
     */
    public function onDataClear():void
    {
        // currently nothing to do
    }

    /**
     * Clears metadata to initial values.
     */
    public function clearMetadata(array $initial = []):void
    {
        $this->setMetadata($initial);
        $this->saveMetadata();
    }

    /**
     * Drops metadata store
     */
    public function dropMetadata():void
    {
        $this->metaData = [];
        $filepath = $this->getFilepath();
        Log::debug(__METHOD__."(): ".$filepath);
        if($filepath) {
            // if there is no filepath, there is nothing to drop
            $fullFilePathName = $filepath.'.json';
            if(file_exists($fullFilePathName)) {
                $success = \unlink($fullFilePathName);
                if($success === false) {
                    throw new \Exception("dropping metadata file failed ".$fullFilePathName);
                }
            }
        }
    }



}
