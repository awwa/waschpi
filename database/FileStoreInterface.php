<?php
/**
 * A file store holds its data in tab separated lines.
 * Each line is a dataset.
 * GPL2 Licence
 */
declare(strict_types=1);

namespace awwa\waschpi\Database;

use Illuminate\Support\Facades\Log;



/**
 * Describes functions to access store files.
 */
interface FileStoreInterface
{
    /**
     * platziere den Dateizeiger auf den Dateianfang und kürze die Datei auf eine Länge von 0.
     * Existiert die Datei nicht, versuche, diese zu erzeugen.
     */
    public const MODE_RW_START_NULL = 'w+';
    /**
     * platziere den Dateizeiger auf den Dateianfang.
     */
    public const MODE_RW_START = 'r+';
    /**
     * Platziere den Dateizeiger auf das Dateiende. Existiert die Datei nicht, versuche, diese zu erzeugen.
     * In diesem Modus wirkt sich fseek() nur auf die Leseposition aus; beim Schreiben wird immer angehängt.
     */
    public const MODE_RW_TAIL = 'a+';
    /**
     * Open the file for RW. If the file does not exist, it is created.
     * If it exists, it is neither truncated (as opposed to 'w'), nor the call to this function fails (as is the case with 'x').
     * The file pointer is positioned on the beginning of the file.
     */
    public const MODE_RW_CREATE = 'c+';


    public function getFileExtension();

}
