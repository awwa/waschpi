<?php
/**
 * Store wash cycles in an file archive.
 * A storage item consists of a data timeline and meta information stored separateley.
 *
 * The store may be created with the new operator or read from file.
 * If it is created as object, it only exists in memory.
 * To manifest its data, it must be intentionally written with one of its methods.
 *
 *
 * GPL2 Licence
 */
namespace awwa\waschpi\Database;

use awwa\waschpi\App\Events\WaschpiEvent;
use awwa\waschpi\App\Models\Washcycles\WashCycle;
use awwa\waschpi\App\Models\Washcycles\WashcycleFormatter;
use Exception;
use awwa\waschpi\Database\TupelStore;
use Illuminate\Support\Facades\Log;
use awwa\waschpi\App\Providers\TimeTrait;


/**
 * Represents one archived wash cycle.
 *
 * Metadata (json) and data series (tsv) of a wash cycle are stored in different files.
 *
 * The name of the file corresponds to the ISO date of the time the cycle is created.
 *
 */
class ArchiveStore
{
    use TimeTrait, FilePathFromTimestampTrait;

    public const WASHCYCLE_METAINFO = 'washCycleMetaInfo';

    protected $timestamp;

    /**
     *
     */
    protected static $storePath;

    /**
     * Name of archive files.
     */
    protected $filename = 'archive';
    /**
     * Is a file pointer
     * @deprecated replaced by Store
     */
    protected $metaFile;
    /**
     * @deprecated replaced by Store
     */
    protected $metaData;
    /**
     * @deprecated replaced by Store
     */
    protected $FileStore;

    protected $Store;

    /**
     * @todo these are the same fields as in temp store
     */
    protected $fields = [
        [
            'name' => 'timestamp',
            'type' => 'int',
        ],
        [
            'name' => 'drumTemperature',
            'type' => 'float',
        ],
        [
            'name' => 'absoluteDrumCount',
            'type' => 'int',
        ],
        [
            'name' => 'rpm',
            'type' => 'int',
        ],
        [
            'name' => 'controlBoxTemperature',
            'type' => 'float',
        ],
        [
            'name' => 'drumHumidity',
            'type' => 'float',
        ],
    ];

    public function __construct()
    {
        $this->Store = new Store();
        $this->Store->setDataFields($this->fields);
    }

    public static function loadFromTimeDate(string $isoTimeDate)
    {
        $filename = self::getFilenameFromTimestamp($isoTimeDate);

        $filePath = self::$storePath.'/'.$filename;
        self::loadFromFile($filePath);
    }

    public static function loadFromFile(string $filePath)
    {
        $Self = new ArchiveStore();
        $Store = Store::loadFromFile($filePath);
        $Self->setStore($Store);

        return $Self;
    }


    public function save()
    {
        $DataStore = $this->getStore();
        $filename = $this->getFilename();
        $DataStore->setFilepath($filename, self::$storePath);
        $DataStore->save();
    }


    public function setStore(Store $Store)
    {
        $this->Store = $Store;
    }

    public function getStore():Store
    {
        return $this->Store;
    }


    public static function setStorePath(string $path):void
    {
        self::$storePath = $path;
        // $this->Store->setStorePath($path);
    }


    public function setMetaItem(string $name, $metadata)
    {
        $this->Store->setMetaItem($name, $metadata);
    }

    public function getMetaItem(string $name)
    {
        return $this->Store->getMetaItem($name);
    }

    public function saveMetadata()
    {
        throw new Exception("should be moved to store");
        $metadata = $this->metaData;
        Log::debug(__METHOD__."(): ".$this->timestamp);
        $this->timestamp = $metadata['timestamp'] ?? $this->timestamp;
        if(!$this->timestamp) {
            // throw new Exception("timestamp is required");
            $this->timestamp = null;
        }
        $filepath = $this->getFilepath();
        Log::debug(__METHOD__."(): writing metadata to ".$filepath);
        file_put_contents($filepath.'.json',json_encode($metadata));
    }


    public function loadMetadata()
    {
        throw new Exception("should be moved to store");
        $filepath = $this->getFilepath();
        Log::debug(__METHOD__."(): getting metadata from ".$filepath);

    }

    /**
     * Clears metadata except timestamp
     */
    public function clearMetadata():void
    {
        throw new Exception("should be moved to store");
        // $initial = ['timestamp'=>$this->timestamp];
        $initial = [];
        $this->setMetaDataArray($initial);
    }

    /**
     * Drops metadata store
     * @todo this could be replaced by meta data object
     */
    public function dropMetadata():void
    {
        throw new Exception("should be moved to store");
        $this->metaData = [];
        $filepath = $this->getFilepath();
        Log::debug(__METHOD__."(): ".$filepath);
        if($filepath) {
            $fullFilePathName = $filepath.'.json';
            if(file_exists($fullFilePathName)) {
                $success = \unlink($fullFilePathName);
                if($success === false) {
                    throw new \Exception("dropping metadata file failed ".$fullFilePathName);
                }
            }
        }
    }

    /**
     * Gets filepath from internal path and timestamp
     */
    public function getFilepath()
    {
        if(!self::$storePath) {
            throw new Exception("storePath is required");
        }
        $path = self::$storePath.'/'.$this->getFilename();
        Log::debug(__METHOD__."(): calculated filepath to ".$path);
        return $path;
    }

    /**
     * Calculates filename with timestamp and returns it.
     */
    public function getFilename()
    {
        $timestamp = $this->getTimestamp();
        if(!$timestamp) {
            // return null;
            throw new Exception("timestamp is required");
        }
        return $this->getFilenameFromTimestamp($timestamp);
    }


    public function getStatus(): array
    {
        throw new \Exception("todo");
    }


    public function addWashCycle(): array
    {
        throw new \Exception("has moved to setWashcycle");
    }

    /**
     * Adds wash cycle to store.
     * Meta info and data series are stored in separate files.
     * @param null|WashCycle $Cycle May be null
     * @return ?int Total count of cycles or null.
     * @throws \Exception
     */
    public function setWashCycle(?WashCycle $Cycle):?int
    {
        if ($Cycle === null) {
            return null;
        }

        $metaInfo = $Cycle->toArrayMetaInfo();
        $this->Store->setMetaItem(self::WASHCYCLE_METAINFO, $metaInfo);

        // extract timestamp; is required
        if(intval($metaInfo['startTimestamp'])) {
            $this->setTimestamp(intval($metaInfo['startTimestamp']));
        }
        else {
            throw new Exception("startTimestamp is required to store wash cycle");
        }

        $eventSeries = $Cycle->toArrayEventSeries();
        foreach($eventSeries as $dataset) {
            $this->Store->addDataset($dataset);
        }

        $totalCount = count($eventSeries);

        $this->save();

        return $totalCount;
    }


    public function getAllEvents(): array
    {
        $events = $this->Store->getAllDatasets();
        return $events;
    }



    /**
     * Could be: match the stored cycle with the given cycle.
     * @param WashCycle
     * @return array[]
     */
    public function getMatchFromCycle($WashCycle):?array
    {
        throw new Exception("must be refactored");

        $aCycleEvents = $WashCycle->getEventSeries();
        $aResult['order'] = WashCycle::getSortOrder($aCycleEvents);

        $TempTable = app()->make(TempStore::class);
        $aCurrentOldestFirst = $TempTable->getAllEvents(null, TempStore::TIMESTAMP_OLDEST_FIRST);

        /** @var WashCycle $WashCycle */
        $aCompareResult = $WashCycle->compareSerieWithSweep($aCurrentOldestFirst);
        $aResult['smallestDelta'] = $aCompareResult['smallestDelta'];
        $aResult['smallestDeltaOffset'] = $aCompareResult['smallestDeltaOffset'];
        // maybe unused:
        $Formatter = new WashcycleFormatter($WashCycle);
        $aResult['metadata'] = $Formatter->toDebugOutput();

        return $aResult;
    }

    /**
     * Could be : compare with another wash cycle
     *
     * The list is sorted best match at first.
     * @param WaschpiEvent[] $aCurrentEvents The events of the running wash cycle. Must be oldest first
     * @param int $count
     * @return array[]
     */
    public function findBestMatches($aCurrentEvents, int $count=10):?array
    {
        throw new Exception("must be refactored");

        // loop all stores cycles and match them with the given events
        $aCycles = $this->getCycles($count);
        $aResults = [];

        $BestMatchingCycle = null;
        foreach ($aCycles as $key => $Cycle) {
            $aResult = [];
            try {
                $aCycleEvents = $Cycle->getEventSeries();
                $aResult['order'] = WashCycle::getSortOrder($aCycleEvents);

                /** @var WashCycle $Cycle */
                $aCompareResult = $Cycle->compareSerieWithSweep($aCurrentEvents);
                $aResult['smallestDelta'] = $aCompareResult['smallestDelta'];
                $aResult['smallestDeltaOffset'] = $aCompareResult['smallestDeltaOffset'];
                $Formatter = new WashcycleFormatter($Cycle);
                $aResult['metadata'] = $Formatter->toDebugOutput();
            }
            catch (\Exception $exc) {
                $aResult['error'] = $exc->getMessage();
            }

            $aResults[] = $aResult;
        }

        // sort best matches (lowest deltasum) at beginning of array
        usort($aResults, function($a, $b) {
            if (!isset($a['smallestDelta'])) {
                return 0;
            }
            if (!isset($b['smallestDelta'])) {
                return 0;
            }
            if ($a['smallestDelta'] == $b['smallestDelta']) {
                return 0;
            }
            return ($a['smallestDelta'] < $b['smallestDelta']) ? -1 : 1;
        });

        return $aResults;
    }




    // public function __destruct()
    // {
    //     $this->setMetadata_to_file();
    //     throw new Exception("make manifest for file store is todo");
    // }

}
