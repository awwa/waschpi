<?php
/**
 * GPL2 Licence
 *
 */

namespace awwa\waschpi\Database;


/**
 * Describes an serializable item.
 */
interface StorableInterface
{
    /**
     * Creates from its storable form.
     * @param array $arr
     * @return StorableInterface
     */
    public static function fromArray(array $arr): StorableInterface;

    /**
     * Converts in its storable form.
     * @return array
     */
    public function toArray():array;
}
