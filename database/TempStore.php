<?php
/**
 * A wash cycle consists of events.
 * Events contain temperatures and rotation counts.
 *
 * GPL2 Licence
 */
declare(strict_types=1);

namespace awwa\waschpi\Database;

use awwa\waschpi\Database\StorableInterface;
use awwa\waschpi\Database\TempStoreInterface;
use awwa\waschpi\App\Events\WaschpiEvent;
use awwa\waschpi\App\Models\Washcycles\WashCycle;
use awwa\waschpi\Database\TempStoreTrait;
use Exception;
use Illuminate\Support\Facades\Log;
use awwa\waschpi\Database\Store;


/**
 * Represents the collection of temporary waschpi events.
 *
 * It holds the temporary, unprocessed events
 *
 */
class TempStore implements TempStoreInterface
{
    use TempStoreTrait, FilePathFromTimestampTrait;

    public const DEFAULT_FILENAME = 'tempstore';

    protected $sizeLimit = 180;

    protected static $storePath;
    /** @var Store */
    protected $Store;

    protected $fields = [
        [
            'name' => 'timestamp',
            'type' => 'int',
        ],
        [
            'name' => 'drumTemperature',
            'type' => 'float',
        ],
        [
            'name' => 'absoluteDrumCount',
            'type' => 'int',
        ],
        [
            'name' => 'rpm',
            'type' => 'int',
        ],
        [
            'name' => 'controlBoxTemperature',
            'type' => 'float',
        ],
        [
            'name' => 'drumHumidity',
            'type' => 'float',
        ],
    ];

    public function __construct()
    {
        // $this->Store = new Store();
        // $this->Store->setFilepath(self::DEFAULT_FILENAME, self::$storePath);
        // $this->Store->setDataFields($this->fields);

        if(empty(self::$storePath)) {
            throw new \Exception(__METHOD__."(): store path must be set to create temp store");
        }
        $filePath = self::$storePath.'/'.self::DEFAULT_FILENAME;
        $this->Store = Store::loadFromFile($filePath ,$this->fields);
    }


    public static function setStorePath($filePath)
    {
        self::$storePath = $filePath;
    }


    public function setStore($Store){
        $this->Store = $Store;
    }

    public function getFileStore():Store
    {
        return $this->Store;
    }

    public function getFields():array
    {
        return $this->fields;
    }


    public static function loadFromFile(string $filePathAndName=self::DEFAULT_FILENAME):TempStore
    {
        $fullFilePath =  self::$storePath .'/'.$filePathAndName;
        $Self = new self();
        $Store = Store::loadFromFile($fullFilePath, $Self->getFields());

        $Self->setStore($Store);
        return $Self;
    }

    /**
     * Calls save() of store.
     */
    public function save()
    {
        return $this->Store->save();
    }

    /**
     * Stores an event in the collection.
     */
    public function addEvent(StorableInterface $Event):array
    {
        $fields = $Event->toArray();
        $result =  $this->Store->addDataset($fields);
        $this->Store->truncate($this->sizeLimit);
        return $result;
    }

    /**
     * Returns the number of entries in the store.
     */
    public function getStoreCount()
    {
        $array = $this->Store->getAllDatasets();
        return count($array);
    }

    /**
     * Gets all events.
     * Events in the store are ordered newest last.
     * @todo evaluate parameter
     * @param int $limit
     * @param int $sort
     * @return WaschpiEvent[] Default is newest first
     */
    public function getAllEvents(int $limit=null, int $sort=self::TIMESTAMP_OLDEST_FIRST)
    {
        if ($sort !== self::TIMESTAMP_OLDEST_FIRST) {
            throw new Exception(__METHOD__."(): sort mode can only be oldest first currently");
        }

        $array = $this->Store->getAllDatasets($limit);

        // convert to WaschpiEvents
        $out = [];
        foreach($array as $dataset) {
            $out[] = WaschpiEvent::fromArray($dataset);
        }

        return $out;
    }


    /**
     * Gets last complete wash cycle and clears the temp table.
     *
     * If the wash cycle is not complete, it is not fetched and the temp table is left untouched.
     *
     * If more than one cycle is detected, only the last is evaluated.
     *
     * The cycle is returned with oldest event first.
     *
     * 1) Detect running wash cycle: a pause longer than 5min is considered as idle state.
     * 2) Harvest all the table and scan for a wash cycle.
     * 3) Strips rpm-zero events
     *
     * @param array $stats Returns debug info
     * @param bool $cleanTable If false, the temp table is not emptied after harvesting.
     * @return WashCycle|null
     */
    public function harvestLastWashCyle(array &$stats=null, $cleanTable=true):?WashCycle
    {
        $stats = [];
        $aDrumRevolves = [];

        /** @var array $aAllEventsOldestFirst sort oldest first */
        $aAllEventsOldestFirst = $this->getAllEvents($this->maxWashCycleDuration, self::TIMESTAMP_OLDEST_FIRST);
        $stats['all event count'] = count($aAllEventsOldestFirst);
        $stats['total analysed events'] = 0;

        // walk through events from the end and find the start of cycle
        $aCycleEvents = [];
        $idleCounter = 0;
        $drumRevolveSum = 0;
        $washCycleIsComplete = false;
        // sorted oldest first
        foreach ($aAllEventsOldestFirst as $Event) {
            $rpm = $Event->getRpm();
            $drumRevolveSum += $rpm;
            $aDrumRevolves[] = $rpm;

            // detect start and end of cycle
            if ($drumRevolveSum >= $this->drumRevolveSumThreshold) {
                $stats['washcycle entered'] = true;
                // Wash cycle was entered.
                // Now, idle events can be counted to detect end of wash cycle
                if ($rpm <= $this->rpmNoiseThreshold) {
                    $idleCounter ++;
                    $stats['had idle events'] = true;
                }
                else {
                    $idleCounter = 0;
                }

                /** @var array $aCycleEvents sorted oldest first */
                $aCycleEvents[] = $Event;

                if ($idleCounter >= $this->maxIdleEventsInWashCycle) {
                    // detection complete, finish
                    $washCycleIsComplete = true;
                    $stats['washcycle complete'] = true;
                    break;
                }
            }

            if ($idleCounter >= $this->maxIdleEventsInWashCycle) {
                // drop noise drum counts
                $drumRevolveSum = 0;
                $stats['dropped noise drum counts'] = true;
            }

            $stats['total analysed events']++;
        }
        //        $stats['drumRevolves'] = $aDrumRevolves;

        $Cycle = null;
        if ($washCycleIsComplete) {
            $aCycleEvents = $this->trimEmptyEvents($aCycleEvents);
            $Cycle = WashCycle::createFromWashCycleArray($aCycleEvents);

            // emtpy temp table
            if ($cleanTable) {
                $this->Store->clearStore();
            }
        }

        return $Cycle;
    }


    /**
     *
     * @param int $referenceTime
     * @return WaschpiEvent|null
     * @throws \Exception
     */
    public function getLastEvent(int $referenceTime=null): ?WaschpiEvent
    {
        $line = $this->Store->getLastDataset();
        if ($line) {
            $WaschpiEvent = WaschpiEvent::fromArray($line);
            return $WaschpiEvent;
        }
        else {
            return null;
        }
    }


    /**
     * Gets current washcycle status and current events.
     * @todo
     * Returns count of idle and running minutes.
     * Only the last 180min are evaluated.
     * The machine is idle if the current event is rpm=0 or
     * the sum of rpm in the last 3 mins is <=3
     *
     * @return array 'currentEvents' Order of current events is newest first
     * @return int 'elapsed' Time since the last state change
     * @return string 'startState' The state, the system has now, can be "running" or "idle"
     * @return string 'currentState' The state before the last state change, can be "running" or "idle"
     */
    public function getCurrentWashCycleStatus()
    {
    }


    /**
     * Cleans data independent of contained data.
     *
     * @todo
     *
     * @param int $age Clean by age of document in seconds
     * @param int $limit  Limit total amount of documents (max 1000)
     * @param bool $dryrun True (default) to only show what would be done
     * @return array
     * @throws Exception
     */
    public function cleanExcessiveData(int $age=0, int $limit=0, bool $dryrun=true)
    {
    }


    public function clearStore()
    {
        $this->Store->clearStore();
    }


    public function dropStore()
    {
        $this->Store->dropStore();
    }


}
