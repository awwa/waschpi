<?php
/**
 * GPL2 Licence
 */
declare(strict_types=1);

namespace awwa\waschpi\Database;

use awwa\waschpi\Database\StorableInterface;
use awwa\waschpi\Database\FileStoreTrait;
use awwa\waschpi\App\Events\WaschpiEvent;
use Exception;
use Illuminate\Support\Facades\Log;


/**
 * Represents a collection of storable datasets.
 * This object has a filename and a path.
 * Not timestamp aware here.
 * Extension is fixed to .tsv
 * Has no key awareness. Store has.
 */
class TupelStore implements FileStoreInterface
{
    use FileStoreTrait;

    protected static $fullPath;
    /** @var array */
    protected $data = [];
    /**
     * Names and types of columns.
     */
    public  $keys = [];
    public  $types = [];


    public function getFileExtension()
    {
        return 'tsv';
    }


    public function setDataFields($fields)
    {
        list($this->keys, $this->types) = self::getKeys($fields);
    }

    /**
     * Stores arrays.
     */
    public function addDataset(array $array):array
    {
        $success = false;
        $isTyped = false;
        // filter for types
        if($this->types) {
            $typed = array_map([TupelStore::class,'mapForType'], $array, $this->types);
            $isTyped = true;
        }
        else {
            $typed = $array;
        }
        // add names
        $isNamed = false;
        if($this->keys) {
            if(count($this->keys) !== count($array)) {
                Log::error(__METHOD__. ": given data size (".count($array).") does not match keys size (".count($this->keys).") and is dropped");
            }
            else {
                $named = array_combine($this->keys, $typed);
                $isNamed = true;
                $success = true;
                $this->data[] = $named;
            }
        }
        else {
            $this->data[] = $typed;
        }

        return [
            'typed' => $isTyped,
            'named' => $isNamed,
            'keyCount' => count($this->keys),
            'dataCount' => count($array),
            'success' => $success,
            'countAfter' => $this->getStoreCount(),
        ];
    }


    public function getStoreCount()
    {
        return count($this->data);
    }

    /**
     * Loads file content and creates the object.
     *
     */
    public static function loadFromFile(string $filePathAndName, array $fields)
    {
        $Self = new self();
        $lineCounter = 0;
        $data = [];

        // load data
        $Self->setFilePath($filePathAndName);
        $filepath = $Self->getFilePath($Self->getFileExtension());
        $fileHandle = \fopen($filepath, self::MODE_RW_CREATE);
        if(!$fileHandle) {
            throw new Exception("filename invalid:".$fileHandle);
        }

        list($Self->keys, $Self->types) = self::getKeys($fields);

        $line = true;
        $lineNumber = 1;
        while($line) {
            $line = \fgetcsv(
                $fileHandle,
                0,
                self::$separator,
                self::$enclosure,
                self::$escapeChar
            );
            if($line) {
                if($line != [null]) {
                    $result = $Self->addDataset($line);
                    if($result['success'] !== true) {
                        Log::error("add dataset failed in Line $lineNumber");
                    }
                }
            }
            $lineNumber ++;
        }
        \fclose($fileHandle);

        return $Self;
    }


    /**
     * splits field description into keys and type list.
     */
    public static function getKeys($fields)
    {
        // prepare fieldname array
        $fieldNames = [];
        $fieldTypes = [];
        foreach ($fields as $i => $dataset) {
            $fieldNames[] = $dataset['name'];
            $fieldTypes[] = $dataset['type'];
        }

        return [$fieldNames, $fieldTypes];
    }

    /**
     * Mapping callback to cast raw data to its datatype.
     */
    protected static function mapForType($value, $type)
    {
        switch ($type) {
            case 'int':
                $value = intval($value);
                break;
            case 'float':
                $value = floatval($value);
                break;
            default:
                //throw new \Exception("unknown type ".$type);
                break;
        }
        return $value;
    }


    protected function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Gets all events.
     * Events in the store are ordered newest last.
     * @todo evaluate parameter
     */
    public function getAllDatasets($limit=null)
    {
        if($limit) {
            $datasets = array_slice($this->data, 0, $limit);
        }
        else {
            $datasets = $this->data;
        }
        return $datasets;
    }


    public function getLastDataset(): ?array
    {
        reset($this->data);
        if ($this->data) {
            return $this->data[count($this->data)-1];
        }
        else {
            return [];
        }
    }

    /**
     * Truncates the store to the given size.
     */
    public function truncate($size)
    {
        $start = count($this->data) - $size;
        if($start > 0) {
            $this->data = array_slice($this->data, $start);
        }
    }

    /**
     * Stores the data to file.
     */
    public function save()
    {
        // data
        $filehandle = \fopen($this->getFilepath(), Self::MODE_RW_START_NULL);
        if (!$filehandle) {
            throw new \Exception(__METHOD__." filehandle is empty");
        }
        foreach($this->data as $data) {
            if(!is_array($data)) {
                // error, data is not inserted
                Log::error(__METHOD__.": given data is not an array ($data) and was dropped");
            }
            elseif(!empty($this->keys) && count($this->keys) !== count($data)) {
                // error, data is not inserted
                Log::error(__METHOD__.": given data size (".count($data).") does not match keys size (".count($this->keys).") and was dropped");
            }
            else {
                $count = fputcsv(
                    $filehandle,
                    $data,
                    Self::$separator,
                    Self::$enclosure,
                    Self::$escapeChar
                );
            }
        }
    }



    /**
     * Empties the data file.
     */
    public function clearStore()
    {
        $filename = $this->getFilepath();
        if(!$filename) {
            // nothing can be done. This is unexpected anyway
        }
        else if (!file_exists($filename)) {
            // nothing to do
        }
        else {
            $cleared = \file_put_contents($filename, "");
            if($cleared === false) {
                throw new \Exception("clearing file store failed for ".\strval($filename));
            }
        }
        $this->data = [];
    }

    /**
     * Deletes the data file.
     */
    public function dropStore()
    {
        // data file
        $filename = $this->getFilepath();
        Log::debug(__METHOD__. "(): ".$filename);
        if ($filename) {
            if (file_exists($filename)) {
                $success = \unlink($filename);
                if($success === false) {
                    throw new \Exception("dropping file store failed for ".\strval($filename));
                }
            }
            else {
                Log::warning(__METHOD__. "(): store file not exists on deletion");
            }
        }
        else {
            Log::warning(__METHOD__. "(): store filename was empty");
        }
    }

}
