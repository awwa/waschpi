<!DOCTYPE html>
<html lang="de">
    <!--
        Waschpi Project
        https://gitlab.com/awwa/waschpi
        GPL2 Licence
    -->
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="favicon.ico">

        <title>{{$projectname}}</title>

        <!-- urls of the following resources should be refacored -->
        <!--<link rel="stylesheet" href="../resources/views/jquery-ui-themes-1.12.1/jquery-ui.css">-->
        <link rel="stylesheet" href="../resources/views/style.css">

        <script src="../vendor/components/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../vendor/components/jqueryui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="../vendor/nnnick/chartjs/dist/Chart.bundle.js"></script>

        <script type="text/javascript" src="lib/numberblock"></script>

        <script type="text/javascript">
            // Numerical time indiator and progress bar
            $(document).ready(function () {
                var progressbarElement = $("div#progressbar-washcycle").progressbar({
                    max: 100,
                    value: false,
                    disabled: true,
                });

                /* numerical time indicator: remaining time to finish */
                var TimeIndicator = $("div#time-to-finish").numberBlock();

/* Status display is disabled
                updateStatus();
                window.setInterval(updateStatus, (60*1000));
*/

                function updateStatus()
                {
                    // update status of time indicator and progress bar
                    $.getJSON("{{$statusurl}}")
                    .done(function (washStatus) {
                        console.log("Status Data: ", washStatus);
                        updateTimeIndicator(washStatus);
                        updateProgressbar(washStatus);
                    })
                    .fail(function (jqxhr, textStatus, error) {
                        var err = textStatus + ", " + error;
                        console.error("Request Failed: " + err);
                        TimeIndicator.setFields('', '???', '');
                        updateTimeIndicator(false);
                        updateProgressbar(false);
                    });
                }

                function updateTimeIndicator(washStatus)
                {
                    TimeIndicator.setFields('', '???', 'min');

                    if (washStatus == false) {
                        TimeIndicator.setFields('', '???', '');
                    }
                    else if (washStatus.startState == 'idle') {
                        TimeIndicator.setStatus("Ruht seit");
                        TimeIndicator.setValue(washStatus.elapsed);
                    } else if (washStatus.startState == 'running') {
                        if (washStatus.remaining < 0) {
                            TimeIndicator.setStatus("verzögert seit");
                        }
                        else {
                            TimeIndicator.setStatus("noch");
                        }
                        TimeIndicator.setValue(washStatus.remaining);
                    } else {
                        TimeIndicator.setFields('ruht', '', '');
                    }
                }

                function updateProgressbar(washStatus)
                {
                    // this removes the animation from the progressbar:
                    progressbarElement.progressbar("option", "value", 0);

                    if (washStatus == false) {
                        progressbarElement.progressbar("option", "disabled", true);
                    }
                    else if (washStatus.startState == 'idle') {
                        progressbarElement.progressbar("disable");
                    } else if (washStatus.startState == 'running') {
                        let progress = (washStatus.elapsed / washStatus.totalduration) * 100;
                        progressbarElement.progressbar("enable");
                        progressbarElement.progressbar("option", "value", progress);
                    } else {
                        progressbarElement.progressbar("disable");
                    }
                };
            });
        </script>

        <script type="text/javascript">
            /* Chart for temperature and rpm */
            $(document).ready(function () {
                let context = document.getElementById('measurementDiagram').getContext('2d');
                let washMonitor = createChart(context);
                updateChart(washMonitor);
                window.setInterval(updateChart, (60*1000), washMonitor);
            });

            /**
             * Creates chart from context and returns it.
             */
            var createChart = function(context)
            {
                var now = (new Date()).getTime();
                var sThreeHoursAgo = now - (3 * 3600);

                var washMonitor = new Chart(context, {
                    type: 'line',
                    data: {
                        datasets: [{
                            data: [],
                            yAxisID: 'rpmAxis',
                            label: 'RPM',
                            pointRadius: 1,
                            pointBorderWidth: 0,
                            spanGaps: true,
                            fill: false,
                            borderColor: 'rgb(99, 255, 132)',
                            backgroundColor: 'rgb(99, 99, 99, 0)',
                        },{
                            data: [],
                            yAxisID: 'tempAxis',
                            label: 'Temperature',
                            borderWidth: 1,
                            pointRadius: 1,
                            pointBorderWidth: 0,
                            spanGaps: true,
                            borderColor: 'rgb(255, 99, 132)',
                            backgroundColor: 'rgb(99, 99, 99, 0)',
                        }]
                    },
                    options: {
                        maintainAspectRatio: false,
                        legend: {
                            display: true,
                        },
                        scales: {
                            yAxes: [{
                                    id: 'rpmAxis',
                                    type: 'logarithmic',
                                    labelString: "RPM",
                                    ticks: {
                                        max: 1200,
                                        min: 0,
                                        stepSize: 100
                                    },
                                },{
                                    id: 'tempAxis',
                                    type: 'linear',
                                    labelString: "temp",
                                    ticks: {
                                        max: 60,
                                        min: 10,
                                        stepSize: 10
                                    },
                                },
                            ],
                            xAxes: [{
                                type: 'time',
                                time: {
                                    bounds: 'data',
                                    displayFormats: {
                                        second: 'H:mm:ss',
                                        minute: 'H:mm',
                                        hour: 'H:mm',
                                        day: 'e D. H',
                                    },
                                },
                                ticks: {
                                    suggestedMin: sThreeHoursAgo,
                                    beginAtZero: false, // minimum value will not be 0.
                                },
                                distribution: 'linear',
                            }]
                        },
                    }
                });
                return washMonitor;
            };

            /**
             * Gets current data listing und updates chart.
             * @param {Chart} chart
             * @returns {undefined}
             */
            var updateChart = function(chart)
            {
                // fill data from api
                $.getJSON("{{$listurl}}", {pageLength: 180})
                .done(function (listData) {
                    console.log("List Data: ", listData);
                    var chartData1 = [];
                    var chartDataTemperature = [];
                    listData.forEach((listItem) => {
                        chartData1.push({
                            t: listItem.timestamp * 1000,
                            y: listItem.rpm,
                        });
                        chartDataTemperature.push({
                            t: listItem.timestamp * 1000,
                            y: listItem.drumTemperature,
                        });
                    });

                    chart.data.datasets[0].data = chartData1;
                    chart.data.datasets[1].data = chartDataTemperature;
                    chart.update();
                })
                .fail(function (jqxhr, textStatus, error) {
                    var err = textStatus + ", " + error;
                    console.error("Request Failed: " + err);
                });
            };

        </script>
    </head>

    <body>
        <noscript>JavaScript ist erforderlich</noscript>
        <div>
            <div id="time-to-finish" class="single-number">
                <span class="numberBlockStatus"></span>&nbsp;<span class="numberBlockValue"></span>&nbsp;<span class="numberBlockUnit"></span>
            </div>
            <div id="progressbar-washcycle" class="progressbar"></div>
        </div>

        <div id="chartjs" class="single-chart">
            <canvas id="measurementDiagram" height="200"></canvas>
        </div>

    </body>

</html>
