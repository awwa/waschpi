/*
 * This component displays a number (called "value") with a prefix (called "status") and a postfix (called "unit").
 * GPL2 Licence
 */
(function ($)
{
    $.fn.numberBlock = function ()
    {
        var self = this;

        this.each(function ()
        {
            console.log("NumberBlock self ", self);
            $(this).css({
                'height': '100%',
            });
            $(this).addClass('waiting').removeClass('ready');
        });

        /**
         * Sets status, value, unit all in one.
         * @param {string} status
         * @param {type} value
         * @param {type} unit
         * @returns {undefined}
         */
        this.setFields = function (status, value, unit)
        {
            console.log("NumberBlock setFields() ", status, value, unit);
            self.setStatus(status);
            self.setValue(value);
            self.setUnit(unit);
        }

        this.setStatus = function (value)
        {
            $(this).find('.numberBlockStatus').text(value);
        }

        this.setValue = function (value)
        {
            $(this).find('.numberBlockValue').text(value);
            $(this).removeClass('waiting').addClass('ready');
        }

        this.setUnit = function (value)
        {
            $(this).find('.numberBlockUnit').text(value);
        }

        return this;
    };
}(jQuery));