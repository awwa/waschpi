# Waschpi is a washing machine monitoring programm.

Its purpose is to show the state of a running wash cycle and calculate an estimation of its time to finish.

## Platform

It was developed to run on an Raspberry Pi 2B with Apache, PHP7 and MongoDB.
It uses one of the Raspberry's I2C Buses with a temperature sensor and a revolution counter on the washing machine drum attached to it.

## How it works

The programm collects data of the state of a washing machine by drum speed and temperature measurement.
If a wash cycle is detected, it is monitored and stored after completion. Also an estimation of the remaining time to wash is calculated.
This is done by comparing the results of a cross-correlation of the current and the last few stored wash cycles.
The wash cycle with the best match is selected and its length is assumed to be the overall length of the current wash cycle.


## Screenshot:

![Screenshot of the UI](doc/waschpi-screenshot_2020-12-02.png "Screenshot 1")
