<?php
/**
 * Test basic functionality of tupel store.
 * The store consists of two files: metadata and tsv data file
 * GPL2 Licence
 */
declare(strict_types=1);

namespace awwa\waschpi\tests;

use Laravel\Lumen\Testing\TestCase;
use Illuminate\Support\Facades\Log;
use awwa\waschpi\Database\JsonDataStore;
use awwa\waschpi\Database\TupelStore;


class TupelStoreTest extends TestCase
{
    /** @var TupelStore */
    protected static $tupelStore;
    protected static $testStorePath = ".";
    protected $testTimestamp = 1500000000;

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        $this->app = require __DIR__.'/../bootstrap/app.php';
        return $this->app;
    }

    public function setUp(): void
    {
        parent::setUp();
        self::$testStorePath = dirname(__FILE__)."/storage";
    }

    /**
     * After the test, the test-store must be empty
     */
    public function tearDown():void
    {
        // directory must be emptied after test
        $dir = self::$testStorePath;
        if ($dir) {
            $filesToDelete = glob("$dir/*");
            Log::info("glob store=".$dir, $filesToDelete);
            foreach($filesToDelete as $filepathname) {
                $success = \unlink($filepathname);
            }
            //$this->assertEquals(0, count(glob("$dir/*")));
        }
    }

    /**
     * Store can be initialized and data can be set.
     */
    public function test01addData()
    {
        self::$tupelStore = new TupelStore();
        $dataset = [4,7,2,8,6,1];
        self::$tupelStore->addDataset($dataset);
        $got = self::$tupelStore->getLastDataset();
        $this->assertEquals($dataset, $got);
    }


    /**
     * Store can create itself from file
     */
    public function test02create()
    {
        $timestamp = 1234560000;
        $filename = self::$testStorePath."/".date('Y-m-d\THi', $timestamp)."archive.tsv";
        $tupelset = [
            [7,5,9,3,4,7],
            [2,6,3,9,8,6]
        ];
        $this->storeDatafile($filename, $tupelset);

        self::$tupelStore = TupelStore::loadFromFile($filename, []);
        $got = self::$tupelStore->getAllDatasets();
        $this->assertEquals($tupelset, $got);
    }


    /**
     * TupelStore can save to file
     */
    public function test03save()
    {
        $metaData = ['new'=>56,'str'=>"testval"];
        $tupelset = [
            [9,1,2],
            [5,7,1],
        ];
        self::$tupelStore = new TupelStore();
        self::$tupelStore->addDataset($tupelset[0]);
        self::$tupelStore->addDataset($tupelset[1]);
        // self::$tupelStore->setMetaData('meta_ns', $metaData);
        // self::$tupelStore->setMetaData('test_string', "test value");

        $timestamp = 1234560000;
        $filename = date('Y-m-d\THi', $timestamp)."archive";
        $filepathname = self::$testStorePath."/".$filename.".tsv";
        self::$tupelStore->setFilepath($filepathname);
        self::$tupelStore->save();

        $got = [];
        $filehandle = \fopen($filepathname, "r");
        $line = true;
        while($line && $line !== [null]) {
            $line = \fgetcsv( $filehandle, 0, "\t", ' ');
            if($line) {
                if($line != [null]) {
                    $got[] = $line;
                }
                // Log::info("got line",$line);
            }
        }
        \fclose($filehandle);

        $this->assertEquals($tupelset, $got);

        // Json meta data file
        // $filepathname = self::$testStorePath."/".$filename.".json";
        // $json = \file_get_contents($filepathname);
        // $this->assertNotEmpty($json);
        // $data = json_decode($json, true);
        // $this->assertArrayHasKey('meta_ns', $data);
        // $this->assertArrayHasKey('test_string', $data);

        // $this->assertEquals($data['meta_ns'], $metaData);
        // $this->assertEquals($data['test_string'], "test value");


    }

    public function test10jsonFile() {
        // create object and save to file
        $JsonFile = new JsonDataStore();
        $JsonFile->set('d1', 11);
        $JsonFile->set('d2', 22);
        $JsonFile->set('d3', 33);
        $JsonFile->setFilepath(dirname(__FILE__)."/storage/json-test-file.txt");
        $JsonFile->saveMetadata();

        // verify file written
        $expectedFilepath = dirname(__FILE__)."/storage/json-test-file.json";
        $this->assertFileExists($expectedFilepath);
        $json = file_get_contents($expectedFilepath);
        $data = json_decode($json, true);
        $expectedData = ['d1'=> 11,'d2'=> 22,'d3'=> 33,];
        $this->assertEquals($expectedData, $data);

        // create object from file
        $JsonFile = JsonDataStore::loadFromFile($expectedFilepath);
        $this->assertEquals($expectedData, $JsonFile->getMetadata());

    }


    protected function storeDatafile($filepathname, $tupelset)
    {
        $filePointer = \fopen($filepathname, "w+");

        foreach($tupelset as $data) {
            $count = fputcsv($filePointer, $data, "\t", ' ');
        }
    }

    protected function clearStore($filename)
    {
        $filePointer = \fopen($filename, "w+");
        $cleared = \fwrite($filePointer, '');
        if($cleared === false) {
            throw new \Exception("clearing file store failed for ".\strval($filename));
        }
    }

    protected function cleanStore($filename)
    {
        $success = \unlink($filename);
        if($success === false) {
            throw new \Exception("clean file store failed for ".\strval($filename));
        }
    }

}
