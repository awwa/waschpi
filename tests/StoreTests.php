<?php

declare(strict_types=1);

namespace awwa\waschpi\tests;

use awwa\waschpi\Database\Store;
use Laravel\Lumen\Testing\TestCase;

/**
 * Tests the Store.
 */
class StoreTests extends TestCase
{
    use StoreTestTrait;

    public function setUp(): void
    {
        parent::setUp();
        $this->cleanTestStore();
    }


    public function tearDown():void
    {
        // todo $dir = dirname($dir);
        // todo $this->assertEquals(0, count(glob("$dir/*")));
        $this->cleanTestStore();
        parent::tearDown();
    }

    /**
     * Test create store with meta file and data file.
     *
     * @return void
     */
    public function test01CreateStore()
    {
        $file = dirname(__FILE__). '/storage/store-test.test';
        $keys = [
            [
                'name' => 'acol',
                'type' => 'int',
            ],
            [
                'name' => 'bcol',
                'type' => 'float',
            ],
            [
                'name' => 'ccol',
                'type' => 'int',
            ],
        ];

        $Store = Store::createOrUpdate($file, $keys);

        $metaFile = $Store->getMetaObject()->getFilepath();
        $this->assertFileExists($metaFile);
        $json = file_get_contents($metaFile);
        $this->assertIsString($json);
        $metaData = json_decode($json, true);
        $this->assertEmpty(json_last_error(), json_last_error_msg());
        $this->assertIsArray($metaData);
        $this->assertArrayHasKey(Store::KEY_META_FIELDS, $metaData);

        $tupelStoreFile = $Store->getFileStore()->getFilepath();
        $this->assertFileExists($tupelStoreFile);
        $lines = file_get_contents($tupelStoreFile);
        $this->assertIsString($lines);
        $this->assertEmpty($lines);
    }

    /**
     * Test create store with meta file and data file.
     *
     * @return void
     */
    public function test02UpdateStore()
    {
        $file = dirname(__FILE__). '/storage/store-test.test';
        $keys = [
            [
                'name' => 'acol',
                'type' => 'int',
            ],
            [
                'name' => 'bcol',
                'type' => 'float',
            ],
            [
                'name' => 'ccol',
                'type' => 'int',
            ],
        ];

        $Store = Store::createOrUpdate($file, $keys);
        $Meta = $Store->getMetaObject();
        $Meta->set('another_meta', [44,33]);
        $Data = $Store->getFileStore();
        $Data->addDataset([77,66,55]);
        $Data->addDataset([11,22,33]);
        $Store->save();

        $metaFile = $Store->getMetaObject()->getFilepath();
        $this->assertFileExists($metaFile);
        $json = file_get_contents($metaFile);
        $this->assertIsString($json);
        $metaData = json_decode($json, true);
        $this->assertIsArray($metaData);
        $this->assertArrayHasKey(Store::KEY_META_FIELDS, $metaData);
        $this->assertArrayHasKey('another_meta', $metaData);
        $this->assertEquals([44,33], $metaData['another_meta']);

        $tupelStoreFile = $Store->getFileStore()->getFilepath();
        $this->assertFileExists($tupelStoreFile);
        $handle = fopen($tupelStoreFile, 'r');
        $line = fgetcsv($handle, 0, "\t");
        $this->assertIsArray($line);
        $this->assertCount(3, $line);
        $this->assertEquals([77,66,55], $line);
        $line = fgetcsv($handle, 0, "\t");
        $this->assertIsArray($line);
        $this->assertCount(3, $line);
        $this->assertEquals([11,22,33], $line);

        // Update should not damage existing data
        $Store = Store::createOrUpdate($file, $keys);
        $Meta = $Store->getMetaObject();
        $Meta->set('update_meta', [88,99]);
        $Data = $Store->getFileStore();
        $Data->addDataset([7.7,6.6,5.5]);
        $Store->save();

        $metaFile = $Store->getMetaObject()->getFilepath();
        $json = file_get_contents($metaFile);
        $this->assertIsString($json);
        $metaData = json_decode($json, true);
        $this->assertIsArray($metaData);
        $this->assertArrayHasKey(Store::KEY_META_FIELDS, $metaData);
        $this->assertCount(3, $metaData[Store::KEY_META_FIELDS]);
        $this->assertEquals([88,99], $metaData['update_meta']);
        $this->assertEquals([44,33], $metaData['another_meta']);

        $tupelStoreFile = $Store->getFileStore()->getFilepath();
        $handle = fopen($tupelStoreFile, 'r');
        $fileData = [];
        $line = true;
        while ($line = fgetcsv($handle, 0, "\t"))
        {
            $fileData[] = $line;
        }
        $this->assertCount(3, $fileData);
        // assert datafile data
        $this->assertEquals(['77','66','55'], $fileData[0]);
        $this->assertEquals(['11','22','33'], $fileData[1]);
        $this->assertEquals(['7','6.6','5'], $fileData[2]);

        $all = $Store->getAllDatasets();
        $this->assertEquals(['acol'=>77,'bcol'=>66,'ccol'=>55,], $all[0]);
        $this->assertEquals(['acol'=>11,'bcol'=>22,'ccol'=>33,], $all[1]);
        $this->assertEquals(['acol'=>7,'bcol'=>6.6,'ccol'=>5,], $all[2]);

    }






}
