<?php
/**
 * Tests wash cycle extraction from temp store.
 * GPL2 Licence
 */
declare(strict_types=1);

namespace awwa\waschpi\tests;


use Laravel\Lumen\Testing\TestCase;
use awwa\waschpi\Database\TempStore;
use awwa\waschpi\tests\WashCycleDataTrait;


final class WashCycleTest extends TestCase
{
    use WashCycleDataTrait;


    public function setUp(): void
    {
        parent::setUp();
        $this->cleanTestStore();
        // Temp table
        self::$tempStore = $this->app->make(TempStore::class);
    }


    public function tearDown():void
    {
        $this->cleanTestStore();
    }

    /**
     * Test of wash cycle simulation.
     */
    public function test0WashCycleSimulation()
    {
        self::createWashCycleSimulation(self::$tempStore, 1601000000, 100, 70);
        $count = self::$tempStore->getStoreCount();
        $this->assertGreaterThan(99, $count);
        $this->assertLessThan(101, $count);

        self::createWashCycleSimulation(self::$tempStore, 1602000000, 10, 70);
        $count = self::$tempStore->getStoreCount();
        $this->assertEquals(110, $count);
    }

    /**
     * Generate a simulation of a complete wash cycle.
     */
    public function test10WashCycleCalculation()
    {
        self::createWashCycleSimulation(self::$tempStore, 1603000000, 100, 70);

        // last event should be idle
        $Event = self::$tempStore->getLastEvent();
        // RPM might be not equal 0 because of simulated noise
        $this->assertLessThan(3, $Event->getRpm());
    }

    /**
     */
    public function test20WashCycleExtraction()
    {
        self::createWashCycleSimulation(self::$tempStore, 1604000000, 50, 60);

        // last event should not be idle
        $Event = self::$tempStore->getLastEvent();
        $this->assertNotEquals(0, $Event->getRpm());

        $all = self::$tempStore->getAllEvents();
        $this->assertCount(50, $all);

        // extract the washcycle from temp table
        // it should be null because the cycle hasnt finished
        $Cycle = self::$tempStore->harvestLastWashCyle();
        $this->assertEquals(null, $Cycle);
    }


    public function test30WashCycleExtraction()
    {
        self::createWashCycleSimulation(self::$tempStore, 1605000000, 150, 120);

        $Event = self::$tempStore->getLastEvent();
        $this->assertEquals(0, $Event->getRpm());

        // extract the (finished) washcycle from temp table
        $Cycle = self::$tempStore->harvestLastWashCyle();
        $this->assertGreaterThan(105, $Cycle->count());
        $this->assertLessThanOrEqual(120, $Cycle->count());
    }

}
