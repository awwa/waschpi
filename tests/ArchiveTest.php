<?php
/**
 * Test basic functionality of archive store.
 * GPL2 Licence
 */
declare(strict_types=1);

namespace awwa\waschpi\tests;

use Laravel\Lumen\Testing\TestCase;
use Illuminate\Support\Facades\Log;
use awwa\waschpi\App\Events\WaschpiEvent;
use awwa\waschpi\App\Models\Washcycles\WashCycle;
use awwa\waschpi\Database\ArchiveStore;
use awwa\waschpi\Database\ArchiveTable;
use awwa\waschpi\Database\TempStore;
use awwa\waschpi\Database\TupelStore;
use Dotenv\Store\FileStore;
use Laravel\Lumen\Testing\DatabaseMigrations;


class ArchiveTest extends TestCase
{
    use WashCycleDataTrait;
    // use DatabaseMigrations;

    protected $testTimestamp = 1500000000;
    /**
     * expected store path
     */
    protected $testStorePath = 'tests/storage';

    public function setUp(): void
    {
        parent::setUp();
        $this->cleanTestStore();
        self::$archiveStore = $this->app->make(ArchiveStore::class);
    }

    /**
     * After the test, the test-store must be empty
     */
    public function tearDown():void
    {
        $this->cleanTestStore();
    }


    /**
     * Tests that a wash events with metadata can be written to the archive
     */
    public function test10()
    {
        $WashCycle = new WashCycle();
        $array = [
            'startTimestamp' => 150000000,
            'endTimestamp' => 150004000,
            'startDateTime' => "2022-10-11T19:23:22",
            'endDateTime' => "2022-10-11T20:33:22",
            'minTemp' => 22,
            'maxTemp' => 66,
            'minHumidty' => 88,
            'maxHumidity' => 99,
            'maxRpm' => 1011,
        ];
        $WashCycle->setMetaInfo($array);
        Log::info("test store content", $array);

        $WpEvent = new WaschpiEvent();
        $WpEvent->setDrumTemperature(55);
        $WpEvent->setRpm(666);
        $WashCycle->addEvent($WpEvent);
        $WpEvent2 = clone $WpEvent;
        $WpEvent2->setDrumTemperature(44);
        $WpEvent2->setRpm(555);
        $WashCycle->addEvent($WpEvent2);

        self::$archiveStore->setWashCycle($WashCycle);

        self::$archiveStore->save();

        // Tests: read back csv file
        $got = [];
        $file = self::$archiveStore->getStore()->getFileStore()->getFilepath();
        $filehandle = \fopen($file, "r");
        $line = true;
        while($line && $line !== [null]) {
            $line = \fgetcsv($filehandle, 0, "\t");
            if($line) {
                if($line != [null]) {
                    $got[] = $line;
                }
                // Log::info("got line",$line);
            }
        }
        \fclose($filehandle);

        // Tests: verify data lines
        $expected = [];
        // 1)
        $wpArray = $WpEvent->toArray();
        foreach ($wpArray as $i => $value) {
            $wpArray[$i] = strval($value);
        }
        $expected[] = array_values($wpArray);
        // 2)
        $wpArray = $WpEvent2->toArray();
        foreach ($wpArray as $i => $value) {
            $wpArray[$i] = strval($value);
        }
        $expected[] = array_values($wpArray);
        $this->assertEquals($expected, $got);

    }


    public function test20FilenameFromTimeCalculation()
    {
        $testTimestamp = 1600000000;
        self::$archiveStore->setTimestamp($testTimestamp);
        self::$archiveStore->save();

        $expectedFile = "tests/storage/2020-09-13T1226archive.json";
        $this->assertFileExists($expectedFile);
        $expectedFile = "tests/storage/2020-09-13T1226archive.tsv";
        $this->assertFileExists($expectedFile);

    }


    public function test30DataLoading()
    {
        $testTimestamp = 1600000000;
        // self::$archiveStore->setTimestamp($testTimestamp);
        $path = env('APP_STORE');
        TempStore::setStorePath($path);
        $TempStore = new TempStore();
        self::createWashCycleSimulation($TempStore, $testTimestamp, 60, 45);
        $WashCycle = $TempStore->harvestLastWashCyle();
        self::$archiveStore->setWashCycle($WashCycle);
        self::$archiveStore->save();
        $washcycleTS = $WashCycle->getMetaInfoKey('startTimestamp');

        $fileToLoad = "tests/storage/".self::$archiveStore->getfilenameFromTimestamp($washcycleTS) .'.json';
        $ArchiveStore = ArchiveStore::loadFromFile($fileToLoad);

        // assert what was loaded
        $this->assertGreaterThan(38, count($ArchiveStore->getAllEvents()) );
        $this->assertLessThan(50, count($ArchiveStore->getAllEvents()) );

        $got = $ArchiveStore->getMetaItem(ArchiveStore::WASHCYCLE_METAINFO)['startTimestamp'];
        $this->assertGreaterThan(0, $got - $testTimestamp);
        $this->assertLessThanOrEqual(500, $got - $testTimestamp);


    }



    /**
     * After the test, the test-store must be emptied
     */
    public function cleanTestStore():void
    {
        $dir = $this->testStorePath;
        $filesToDelete = glob("$dir/*");
        Log::info("glob store=".$dir, $filesToDelete);
        foreach($filesToDelete as $filepathname) {
            $success = \unlink($filepathname);
        }
    }

}
