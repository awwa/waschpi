<?php
/**
 * Harvests temp store and creates simulated wash cycles
 *
 * GPL2 Licence
 */

namespace awwa\waschpi\tests;

use awwa\waschpi\App\Events\WaschpiEvent;
use awwa\waschpi\App\Models\Washcycles\WashCycle;
use awwa\waschpi\Database\TempStoreInterface;
use awwa\waschpi\Database\TempStore;
use Illuminate\Support\Facades\Log;
use awwa\waschpi\Database\ArchiveStore;

/**
 * Creates simulated wash cycle data.
 */
trait WashCycleDataTrait
{
    /** @var TempStore */
    protected static $tempStore;
    /** @var ArchiveStore */
    protected static $archiveStore;


    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        $this->app = require __DIR__.'/../bootstrap/app.php';
        return $this->app;
    }

    /**
     * Transfers completed wash cycles from temp table to archive.
     */
    public static function harvest(TempStoreInterface $TempTable, ArchiveStore $ArchiveTable)
    {
        $aStats = null;
        $Cycle = $TempTable->harvestLastWashCyle($aStats);
        $ArchiveTable->addWashCycle($Cycle);
    }

    /**
     * Generate a simulation of a wash cycle according to parameters.
     *
     * P2 determines how much of the cycle is calculated.
     * The unit of duration, length and offset are minutes.
     *
     * @param TempTable $Table
     * @param int $startTime Start time of wash cycle as timestamp
     * @param int $duration Actual duration of the simulation including offset. If smaller than wash cycle length, the cycle is not completed.
     * @param int $washCycleLength  The scheduled length of the washcycle
     * @param int $offset An idle period before the wash cycle. Is part of duration time.
     */
    public static function createWashCycleSimulation(TempStoreInterface $Table, int $startTime, int $duration, int $washCycleLength, int $offset=0):void
    {
        // initial values
        // $timestamp = mktime(date('H', $startTime), date('i', $startTime), 0) - ($duration*60);
        $timestamp = $startTime;
        $drumCount =1000;
        $boxTemp = 25;
        $drumTemp = 22;
        $washTemp = 55;
        $drumHumid = 70;

        $prevEvent = null;
        for($i=-$offset; $i<$duration; $i++) {
            $percentRunned = floor(($i/$washCycleLength)*100);
            if ($i < 0) {
                // idle time before
                // add drumCount noise
                $rand = rand(0,100);
                $drumCount += ($rand>86) ? intval(($rand-86)/10):0;
            }
            elseif( $percentRunned > 100) {
                // idle time after wash
                // add drumCount noise
                $rand = rand(0,10);
                $drumCount += ($rand>8) ? intval($rand-8):0;
            }
            elseif( $percentRunned > 70) {
                // spin-dry
                $drumCount += 200;
            }
            elseif($percentRunned > 40) {
                // main-wash
                $drumCount += 20;
                if($drumTemp < $washTemp) $drumTemp += 8;
            }
            elseif($percentRunned > 10) {
                // pre-wash
                $drumCount += 10;
                if($drumTemp < $washTemp) $drumTemp += 8;
            }
            elseif($percentRunned > 0) {
                // filling water
                $drumCount += 2;
            }
            else {
            }
            if($drumTemp > 22) $drumTemp --;
            if($drumTemp > 33) $drumTemp --;
            if($drumTemp > 44) $drumTemp --;
            if($drumTemp > 55) $drumTemp --;

            $Event = WaschpiEvent::create($timestamp, $drumCount, $boxTemp, $drumTemp, $drumHumid);
            $Event->setPreviousEvent($prevEvent);
            $Table->addEvent($Event);
            $prevEvent = clone $Event;

            $timestamp += 60;
        }
    }


    protected static function getRandomWaschpiEvent($timestamp=null, $drumCount=null, $boxTemp=null, $drumTemp=null, $drumHumid=null)
    {
        // $timestamp = ;
        // $drumCount = ;
        // $boxTemp = ;
        // $drumTemp = ;
        // $drumHumid;

        $Event = WaschpiEvent::create($timestamp??1600000000, $drumCount??456, $boxTemp??22, $drumTemp??33, $drumHumid??77);
        return $Event;
    }

    /**
     * After the test, the test-store must be emptied
     */
    public function cleanTestStore():void
    {
        $dir = env('APP_STORE');
        $filesToDelete = glob("$dir/*");
        Log::info("glob store=".$dir, $filesToDelete);
        foreach($filesToDelete as $filepathname) {
            $success = \unlink($filepathname);
        }
    }

}
