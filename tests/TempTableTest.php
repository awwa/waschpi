<?php
/**
 * Test basic functionality of temp store.
 * GPL2 Licence
 */
declare(strict_types=1);

namespace awwa\waschpi\tests;

use Laravel\Lumen\Testing\TestCase;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\App;
use awwa\waschpi\Database\TempStore;
use awwa\waschpi\App\Events\WaschpiEvent;
use awwa\waschpi\Database\Store;

class TempTableTest extends TestCase
{
    use WashCycleDataTrait;


    /**
     * Creates simulated wash cycles in archive table.
     */
    public function setUp(): void
    {
        parent::setUp();
        // Temp table
        self::$tempStore = $this->app->make(TempStore::class);
        $this->cleanTestStore();
    }


    public function tearDown():void
    {
        // todo $dir = dirname($dir);
        // todo $this->assertEquals(0, count(glob("$dir/*")));
        $this->cleanTestStore();
    }

    /**
     * Test store is empty
     */
    public function test10getAll()
    {
        $all = self::$tempStore->getAllEvents();
        Log::info("test store content", $all);
        $this->assertEquals(0, self::$tempStore->getStoreCount(), "temp store must be empty");
    }

    /**
     * Test get all function
     */
    public function test20getEvents()
    {
        $Event = new WaschpiEvent();
        $Event->setDrumTemperature(33);
        $Event->setAbsoluteDrumCount(1234);
        $Event->setControlBoxTemperature(22);
        $Event->setDrumHumidity(66);
        self::$tempStore->addEvent($Event);
        $Event->setControlBoxTemperature(23);
        $Event->setDrumHumidity(99);
        self::$tempStore->addEvent($Event);

        $allEvents = self::$tempStore->getAllEvents();

        $this->assertCount(2, $allEvents);
        $this->assertInstanceOf(WaschpiEvent::class, $allEvents[0]);
        $this->assertInstanceOf(WaschpiEvent::class, $allEvents[1]);

        $this->assertEquals(33, $allEvents[0]->getDrumTemperature());

    }

    /**
     * Tests, that events can be stored
     */
    public function test30addEvent()
    {
        $Event = new WaschpiEvent();
        $Event->setDrumTemperature(33);
        $Event->setAbsoluteDrumCount(1234);
        $Event->setControlBoxTemperature(22);
        $Event->setDrumHumidity(66);
        self::$tempStore->addEvent($Event);
        // Log::info("tsv file content=",self::$tempStore->getAllEvents());
        $this->assertEquals(1, self::$tempStore->getStoreCount(), "temp store must have expected count");

        $Event->setControlBoxTemperature(23);
        $Event->setDrumHumidity(99);
        self::$tempStore->addEvent($Event);
        $this->assertEquals(2, self::$tempStore->getStoreCount(), "temp store must have expected count");
    }

    /**
     * Tests, that events can be retrieved
     */
    public function test40getLastEvent()
    {
        $Event = new WaschpiEvent();
        $Event->setDrumTemperature(33);
        $Event->setAbsoluteDrumCount(1234);
        $Event->setControlBoxTemperature(22);
        $Event->setDrumHumidity(66);
        self::$tempStore->addEvent($Event);
        $Event->setControlBoxTemperature(23);
        $Event->setDrumHumidity(99);
        self::$tempStore->addEvent($Event);
        $this->assertEquals(2, self::$tempStore->getStoreCount(), "temp store must have expected count");

        $LastEvent = self::$tempStore->getLastEvent();
        Log::info("last event 1 as array=", $LastEvent->toArray());
        $this->assertEquals(99, $LastEvent->getDrumHumidity());
        $this->assertEquals(1234, $LastEvent->getAbsoluteDrumCount());

        $Event->setDrumTemperature(60);
        $Event->setDrumHumidity(40);
        self::$tempStore->addEvent($Event);
        $LastEvent = self::$tempStore->getLastEvent();
        Log::info("last event 2 as array=", $LastEvent->toArray());
        $this->assertEquals(40, $LastEvent->getDrumHumidity());
        $this->assertEquals(1234, $LastEvent->getAbsoluteDrumCount());
        $this->assertEquals(60, $LastEvent->getDrumTemperature());
    }

    /**
     * Tests, that the store can be created from file
     */
    public function test40createStore()
    {
        $dir = env('APP_STORE');
        TempStore::setStorePath($dir);
        $TempStore = new TempStore();

        // create data file
        $expectData = [
            [3,2,1,6,5,4],
            [9,8,7,6,5,4]
        ];
        $file = $TempStore->getFileStore()->getFileStore()->getFilepath();
        $filehandle = \fopen($file, "w");
        foreach($expectData as $line) {
            fputcsv($filehandle, $line, "\t", " ");
        }
        \fclose($filehandle);

        // create metafile
        $fields = $TempStore->getFields();
        $expectMeta = [
            Store::KEY_META_FIELDS => $fields,
        ];
        $json = json_encode($expectMeta);
        $file = $TempStore->getFileStore()->getMetaObject()->getFilepath();
        file_put_contents($file, $json);

        // Tests
        $TempStore = TempStore::loadFromFile();
        $meta = $TempStore->getFileStore()->getMetaObject()->get(store::KEY_META_FIELDS);
        $this->assertCount(6, $meta);
        $this->assertEquals('int', $meta[0]['type']);
        $data = $TempStore->getFileStore()->getFileStore()->getAllDatasets();
        $this->assertCount(2, $data);
        $this->assertEquals(7, $data[1]['absoluteDrumCount']);

    }

    /**
     * Tests, that the store can be saved to file
     */
    public function test42saveStore()
    {
        $dir = env('APP_STORE');
        TempStore::setStorePath($dir);
        $TempStore = $this->app->make(TempStore::class);
        $ts = 1600300000;
        self::createWashCycleSimulation($TempStore, $ts, 15, 10);
        $TempStore->save();

        // Tests: read back csv file
        $got = [];
        $file = $TempStore->getFileStore()->getFileStore()->getFilepath();
        $filehandle = \fopen($file, "r");
        $line = true;
        while($line && $line !== [null]) {
            $line = \fgetcsv($filehandle, 0, "\t",  ' ');
            if($line) {
                if($line != [null]) {
                    $got[] = $line;
                }
            }
        }
        \fclose($filehandle);
        $TempStore->save();

        // Tests: verify data lines
        $expected = [];
        $this->assertCount(15, $got);

    }

    /**
     * Tests, that the store limits itself
     */
    public function test50truncate()
    {
        $dir = env('APP_STORE');
        TempStore::setStorePath($dir);
        /** @var TempStore $TempStore */
        $TempStore = $this->app->make(TempStore::class);
        $ts = 1600300000;

        for($i=1; $i<=200; $i++) {
            $Event = $this->getRandomWaschpiEvent($ts);
            $TempStore->addEvent($Event);
            // $all = $TempStore->getAllEvents();
            // if(!($i%10)) {
            //     $TempStore->save();
            // }
            $ts += 60;
        }
        $TempStore->save();

        $this->assertEquals(180, $TempStore->getStoreCount());

    }

}
