<?php
/**
 * Tests evaluation and comparison of archived cycles with a current cycle.
 * GPL2 Licence
 */
declare(strict_types=1);

namespace awwa\waschpi\tests;

use awwa\waschpi\App\Models\Washcycles\WashCycle;
use awwa\waschpi\Database\ArchiveStore;
use Laravel\Lumen\Testing\TestCase;
use awwa\waschpi\Database\TempStore;
use awwa\waschpi\tests\WashCycleDataTrait;
use Illuminate\Support\Facades\Log;


final class WashCycleCompareTest extends TestCase
{
    use WashCycleDataTrait;

    public function setUp(): void
    {
        parent::setUp();
        $this->cleanTestStore();
        // Temp table
        self::$tempStore = $this->app->make(TempStore::class);
        // Archive table
        self::$archiveStore = $this->app->make(ArchiveStore::class);
    }


    public function tearDown(): void
    {
        // store must be empty
        $dir = self::$archiveStore->getFilepath();
        $dir = dirname($dir);
        $this->assertEquals(0, count(glob("$dir/*")));
        //$this->cleanTestStore();
    }

    /**
     * Tests the archive store object
     */
    public function test01()
    {
        $this->markTestSkipped("wash cycle compare is not working yet");

        $this->assertNotEmpty(self::$archiveStore->getFilepath());
        $this->assertNotEmpty(self::$archiveStore->getDataStore()->get);

        $value = config('logging.default');
        $this->assertEquals("single", $value);
    }


    public function test10WashCycleCompare()
    {
        $this->markTestSkipped("wash cycle compare is not working yet");

        // 1) create a complete washcycle
        self::createWashCycleSimulation(self::$tempStore, 1594000000, 80, 40, 1);

        // 2) extract the washcycle from temp table
        $RefCycle = self::$tempStore->harvestLastWashCyle();
        $this->assertGreaterThan(30, $RefCycle->count());

        // 3) create a second, unfinished cycle and compare
        self::createWashCycleSimulation(self::$tempStore, 1594100000, 60, 40, 1);
        $aOldestFirst = self::$tempStore->getAllEvents();
        // Log::info("reference wash cycle", $RefCycle->toArray());

        // 4) compare with offset 0
        $result = $RefCycle->compareSerieWithOffset($aOldestFirst, 0);
        $this->assertNotEmpty($result['loopCount']);
        $this->assertLessThan(200, $result['deltasum']);

        // 5) compare with explicit sliding offset
        $aResult = [];
        for ($index = -2; $index <= 2; $index++) {
            $result = $RefCycle->compareSerieWithOffset($aOldestFirst, $index);
            $aResult[] = "{$result['offset']} {$result['deltasum']}";
        }

        // 6) compare with implicit sliding offset
        $aResult = $RefCycle->compareSerieWithSweep($aOldestFirst);
//        echo json_encode($aResult, JSON_PRETTY_PRINT);

//        $this->assertNotEmpty($aResult['smallestDelta']);
        $this->assertNotEmpty($aResult['smallestDeltaOffset']);


        // 7) compare with temp table function
        $aResult = $RefCycle->compareWithTempTable(self::$tempStore);

    }

    /**
     * @todo evaluate findBestMatches()
     */
    public function test20WashCycleCompare()
    {
        $this->markTestSkipped("wash cycle compare is not working yet");

        // 1) create a complete washcycle
        self::createWashCycleSimulation(self::$tempStore, 1594000000, 80, 40, 1);
        // extract the washcycle from temp table
        $Cycle = self::$tempStore->harvestLastWashCyle();
        $this->assertInstanceOf(WashCycle::class, $Cycle);
        $this->assertGreaterThan(30, $Cycle->count());
        // add to archive
        self::$archiveStore->addWashCycle($Cycle);
        // 2)
        self::createWashCycleSimulation(self::$tempStore, 1594100000, 80, 50, 10);
        $Cycle = self::$tempStore->harvestLastWashCyle();
        $this->assertGreaterThan(30, $Cycle->count());
        self::$archiveStore->addWashCycle($Cycle);
        // 3)
        // self::createWashCycleSimulation(self::$tempStore, 1594300000, 60, 43, 3);
        // $Cycle = self::$tempStore->harvestLastWashCyle();
        // $this->assertGreaterThan(30, $Cycle->count());
        // self::$archiveStore->addWashCycle($Cycle);
        // 4)
        // self::createWashCycleSimulation(self::$tempStore, 1594400000, 60, 30, 9);
        // $Cycle = self::$tempStore->harvestLastWashCyle();
        // $this->assertGreaterThan(20, $Cycle->count());
        // self::$archiveStore->addWashCycle($Cycle);

        // // 5) create unfinished cycle and evaluate it
        // self::createWashCycleSimulation(self::$tempStore, 1595000000, 30, 42, 3);
        // $aCurrentEvents = self::$tempStore->getAllEvents();
        // $aOldestFirst = array_reverse($aCurrentEvents);

        // $aResult = self::$archiveStore->findBestMatches($aOldestFirst);
        //echo json_encode($aResult, JSON_PRETTY_PRINT);

    }


}
