<?php
/**
 * Harvests temp store and creates simulated wash cycles
 *
 * GPL2 Licence
 */

namespace awwa\waschpi\tests;

use awwa\waschpi\App\Events\WaschpiEvent;
use awwa\waschpi\Database\TempStore;
use Illuminate\Support\Facades\Log;
use awwa\waschpi\Database\ArchiveStore;

/**
 * Helper functions to test the store.
 */
trait StoreTestTrait
{
    /** @var TempStore */
    protected static $tempStore;
    /** @var ArchiveStore */
    protected static $archiveStore;


    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        $this->app = require __DIR__.'/../bootstrap/app.php';
        return $this->app;
    }


    protected function storeDatafile($filepathname, $tupelset)
    {
        $filePointer = \fopen($filepathname, "w+");

        foreach($tupelset as $data) {
            $count = fputcsv(
                $filePointer,
                $data,
                "\t",
                '"'
            );
        }
    }


    /**
     * After the test, the test-store must be emptied
     */
    public function cleanTestStore():void
    {
        $dir = 'tests/storage';
        $filesToDelete = glob("$dir/*");
        Log::info("glob store=".$dir, $filesToDelete);
        foreach($filesToDelete as $filepathname) {
            $success = \unlink($filepathname);
        }
    }

}
