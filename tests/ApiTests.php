<?php
/**
 * Test api functions.
 * GPL2 Licence
 */
declare(strict_types=1);

namespace awwa\waschpi\tests;

use awwa\waschpi\App\Events\WaschpiEvent;
use awwa\waschpi\App\Models\WashingMachine;
use Laravel\Lumen\Testing\TestCase;
use Mockery;
use awwa\waschpi\Database\TempStore;



class ApiTests extends TestCase
{
    use WashCycleDataTrait;

    use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

    public static function setUpBeforeClass(): void
    {
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->cleanTestStore();
    }

    /**
     * After the test, the test-store must be empty
     */
    public function tearDown():void
    {
        $this->cleanTestStore();
    }

    /**
     * Tests version output
     * @return void
     */
    public function test10Basic()
    {
        $Resp = $this->get('/version');
        $response = $Resp->response->getContent();
        $this->assertNotEmpty($response);
        $this->assertStringContainsString('Lumen', $response);
        $this->assertStringContainsString('8.', $response);
    }

    /**
     * Tests direct measurement
     * @return void
     */
    public function test20CurrentFromTempStore()
    {
        $storageFolder = env('APP_STORE');
        $filename = WashingMachine::getSimulationDataFileName();
        $source = dirname(__FILE__, 2).'/'.$storageFolder.'/../data/'.$filename;
        $target = dirname(__FILE__, 2).'/'.$storageFolder.'/'.$filename;
        copy($source, $target);
        // 1600300000	22	1000	0	25	70

        $this->get('/current')
             ->seeJson([
                'timestamp' => 1600300000,
                'drumTemperature' => 22,
                'absoluteDrumCount' => 1000,
                //
                'rpm' => 0,
                'controlBoxTemperature' => 25,
                'drumHumidity' => 70,
             ]);
    }

    /**
     * Tests measurement listing (events).
     * @return void
     */
    public function test30DataSeriesFromTempStore()
    {
        // prepare temp store
        $TempStore = $this->app->make(TempStore::class);
        $timestamp = 1600500000;
        self::createWashCycleSimulation($TempStore, $timestamp, 20, 16);
        $TempStore->save();

        // test temp store file
        $dir = env('APP_STORE');
        $storeFile = "$dir/tempstore.tsv";
        $this->assertFileExists($storeFile);
        $data = file($storeFile);
        $this->assertCount(20, $data);

        $Resp = $this->get('/events');
        $decodedResponse = json_decode($Resp->response->getContent(), true);
        $this->assertIsArray($decodedResponse);
        $this->assertCount(20, $decodedResponse);
        $event1 = array_shift($decodedResponse);
        $this->assertCount(6, $event1);
        $this->assertArrayHasKey('absoluteDrumCount', $event1);
        $this->assertArrayHasKey('controlBoxTemperature', $event1);
        $this->assertArrayHasKey('drumHumidity', $event1);
        $this->assertArrayHasKey('drumTemperature', $event1);
        $this->assertArrayHasKey('rpm', $event1);
        $this->assertArrayHasKey('timestamp', $event1);

    }

    /**
     * Tests data logging function (eventLogger).
     * @return void
     */
    public function test40Measurement()
    {
        // prepare temp store
        $TempStore = $this->app->make(TempStore::class);
        $all = $TempStore->getAllEvents();
        // check is empty
        $this->assertIsArray($all);
        $this->assertCount(0, $all);

        // place washing machine mock data
        $ts = 1600100000;
        $WpEvent = WaschpiEvent::create($ts, 6324, 22, 33, 77);
        $wpArray = $WpEvent->toArray();
        $file = WashingMachine::getSimulationDataFile();
        $handle = fopen($file, 'w');
        $written = fputcsv($handle, $wpArray, "\t", " ");
        $success = fclose($handle);

        // Log an event
        $Resp = $this->get('/eventLogger');
        $decodedResponse = json_decode($Resp->response->getContent(), true);
        $this->assertIsArray($decodedResponse);
        $this->assertArrayHasKey('typed',$decodedResponse);
        $this->assertEquals(1, $decodedResponse['countAfter']);

        $all = $TempStore->getAllEvents();
        $this->assertCount(1, $all);
    }

    /**
     * Tests data harvesting function (eventLogger).
     * @return void
     */
    public function test50StoreWashCycle()
    {
        // prepare temp store
        /**@var TempStore $TempStore */
        $TempStore = $this->app->make(TempStore::class);
        $timestamp = 1600600000;
        self::createWashCycleSimulation($TempStore, $timestamp, 36, 30);

        // place washing machine mock data
        $Last = $TempStore->getLastEvent();
        $ts = $Last->getTimestamp();
        $ts += 60;
        $WpEvent = clone $Last;
        $WpEvent->setTimestamp($ts);

        $wpArray = $WpEvent->toArray();
        $file = WashingMachine::getSimulationDataFile();
        $handle = fopen($file, 'w');
        $written = fputcsv($handle, $wpArray, "\t", " ");
        $success = fclose($handle);

        // Log an event
        // This should trigger harvesting the temp store
        $Resp = $this->get('/eventLogger');
        $decodedResponse = json_decode($Resp->response->getContent(), true);

        $this->assertIsArray($decodedResponse);

        $dir = env('APP_STORE');
        $storeFile = glob("$dir/*archive.tsv");
        $this->assertCount(1, $storeFile);
        $metaFile = glob("$dir/*archive.json");
        $this->assertCount(1, $metaFile);
    }

    /**
     * Tests simulation data creation.
     * @return void
     */
    public function test60Simulation()
    {
        $time = 1600100000;
        // prepare temp store
        $TempStore = $this->app->make(TempStore::class);
        $all = $TempStore->getAllEvents();
        $countBefore = count($all);
        // check is empty
        $this->assertEquals(0, $countBefore);

        $Resp = $this->get("/simulation?time=$time&exec=30&length=60");
        $decodedResponse = json_decode($Resp->response->getContent(), true);

        $this->assertIsArray($decodedResponse);

        $all = $TempStore->getAllEvents();
        $countAfter = count($all);
        // check is filled
        $this->assertCount(30, $all);
        $this->assertGreaterThan($countBefore, $countAfter);

        // test temp store file
        $dir = env('APP_STORE');
        $storeFile = "$dir/tempstore.tsv";
        $this->assertFileExists($storeFile);
        $data = file($storeFile);
        $this->assertCount(30, $data);

    }

    /**
     * Tests simulation data creation.
     * @return void
     */
    public function test62Simulation()
    {
        $time = 1600100000;
        // prepare temp store
        $TempStore = $this->app->make(TempStore::class);
        $all = $TempStore->getAllEvents();
        $countBefore = count($all);
        // check is empty
        $this->assertEquals(0, $countBefore);

        $Resp = $this->get("/simulation?time=$time&exec=30&length=60");

        $dir = env('APP_STORE');
        $storeFile = "tempstore.tsv";
        $TempStore = TempStore::loadFromFile($storeFile);
        $all = $TempStore->getAllEvents();
        $this->assertCount(30, $all);

        // repeat command should remove previous data
        $Resp = $this->get("/simulation?time=$time&exec=20&length=10");

        $TempStore = TempStore::loadFromFile($storeFile);
        $all = $TempStore->getAllEvents();
        $this->assertCount(20, $all);


    }

}
