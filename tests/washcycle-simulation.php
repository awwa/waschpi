<?php
/**
 * Test script to create, harvest or list washcycles.
 * The Mode is controlled by the GET parameter "function".
 * Possible values are: create, harvest or list.
 *
 * "create" creates a simulated washcycle.
 *
 * "harvest" fetches a washcylcle from temp table (if ready), puts it to the archive and displays it.
 * DB is test DB, but can be changed to production DB.
 *
 * "list" reads the temp table and shows the last complete washcycle
 *
 * @todo Fix "list" function
 * 
 * GPL2 Licence
 */

declare(strict_types=1);

namespace awwa\waschpi\tests;

require_once dirname(__FILE__, 1).'/../vendor/autoload.php';

use awwa\waschpi\Database\TempTable;
use awwa\waschpi\Database\ArchiveTable;
use awwa\waschpi\tests\Config;
use awwa\waschpi\App\Events\WaschpiEvent;
use awwa\waschpi\App\Events\WaschpiEventFormatterDebug;

$Config = new Config();
$TestDb = $Config->getMongoDB();
$TempTable = new TempTable();
$TempTable->setMongoDb($TestDb);

if ( $_GET['function'] === 'create' ) {
    $TempTable->cleanTable();
    //                                           exec time, cycle length, offset=0
    TableTest::createWashCycleSimulation($TempTable, time(), 70, 40, 20);
    $aResult = $TempTable->getAllEvents(999);
}
elseif ( $_GET['function'] === 'harvest' ) {
    $aStats = null;
    $Cycle = $TempTable->harvestLastWashCyle($aStats);
    var_dump($aStats);
    $ArchiveTable = new ArchiveTable();
    $ArchiveTable->addWashCycle($Cycle);
    if ($Cycle) {
        $aResult = $Cycle->getEventSeries();
    }
    else {
        $aResult = [];
    }
}
elseif ( $_GET['function'] === 'list' ) {
    $function = new ListCyclesFunction();
    $aResult = $function->execute();
}
else  {
    $aResult = $TempTable->getAllEvents(999);
}

var_dump(['remaining events in temp table' => count($TempTable->getAllEvents())]);

foreach ($aResult as $Result) {
    if (is_a($Result, WaschpiEvent::class)) {
        /** @var WaschpiEvent $Result */
        $Formatter = new WaschpiEventFormatterDebug();
        echo $Formatter->format($Result),"<br>\n";
    }
    else {
        var_dump($Result);
    }

}


